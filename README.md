定位于企业信息化领域，集成了目前主流的开源框架，采用MVC分层设计，使用Apache License 2.0协议<br/>
是一个高性能、强安全、易扩展的企业级快速开发平台，代码简洁，模块清晰。从 2.0 开始，平台以 SpringBoot 2.X 作为整体架构，简化配置

演示：<br/>
    &nbsp;&nbsp;&nbsp;&nbsp;地址：[http://115.29.173.71/well](http://115.29.173.71/well)<br/>
    &nbsp;&nbsp;&nbsp;&nbsp;账号：test<br/>
    &nbsp;&nbsp;&nbsp;&nbsp;密码：123456<br/>

感谢  **Ruoyi** 、 **Mybatis Mapper** 、 **PageHelper**  开源项目