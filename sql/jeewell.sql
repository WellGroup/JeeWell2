/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1
Source Server Version : 50559
Source Host           : 127.0.0.1:3306
Source Database       : jeewell

Target Server Type    : MYSQL
Target Server Version : 50559
File Encoding         : 65001

Date: 2018-08-23 11:34:50
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for dictionary
-- ----------------------------
DROP TABLE IF EXISTS `dictionary`;
CREATE TABLE `dictionary` (
  `id` varchar(32) NOT NULL COMMENT '主键',
  `parent_id` varchar(32) DEFAULT NULL COMMENT '父级主键',
  `type` int(10) DEFAULT NULL COMMENT '字典类型',
  `type_name` varchar(255) DEFAULT NULL COMMENT '字典类型名称',
  `value` varchar(100) NOT NULL COMMENT '字典值',
  `text` varchar(200) NOT NULL COMMENT '字典文本',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `insert_user` varchar(32) DEFAULT NULL,
  `insert_date` datetime DEFAULT NULL COMMENT '新增时间',
  `update_user` varchar(32) DEFAULT NULL COMMENT '修改人的id',
  `update_date` datetime DEFAULT NULL COMMENT '修改时间',
  `enable` int(2) DEFAULT NULL COMMENT '是否可用，0-禁用，1-可用',
  `memo` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dictionary
-- ----------------------------
INSERT INTO `dictionary` VALUES ('4a33a4f321e34772a2f1dbedce100f58', null, '1', '编程语言', 'Java', 'Java语言', null, null, '2018-06-07 20:28:22', null, '2018-06-07 20:28:22', '1', null);
INSERT INTO `dictionary` VALUES ('aa4a4c36f2444da78b2f4f6ba8466566', null, '2', '爱好', 'Chinese Chess', '中国象棋', null, null, '2018-06-07 20:29:25', null, '2018-06-07 20:29:25', '1', null);
INSERT INTO `dictionary` VALUES ('b00bc0a3a90543fdb2823ba986d28d60', null, '1', '编程语言', 'C++', 'C++语言', null, null, '2018-06-07 20:28:22', null, '2018-06-07 20:28:22', '1', null);
INSERT INTO `dictionary` VALUES ('bd6a4bdc27174d889b0dfbc21f933e40', null, '2', '爱好', 'Basketball', '篮球', null, null, '2018-06-07 20:29:25', null, '2018-06-07 20:29:25', '1', null);
INSERT INTO `dictionary` VALUES ('cd8e878719224f3886281872811faf99', null, '2', '爱好', 'Movie', '电影', null, null, '2018-06-07 20:29:24', null, '2018-06-07 20:29:24', '1', null);
INSERT INTO `dictionary` VALUES ('ea51766f4b124b519397f6e713300c25', null, '1', '编程语言', 'C', 'C语言', null, null, '2018-06-07 20:28:22', null, '2018-06-07 20:28:22', '1', null);

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` varchar(36) CHARACTER SET utf8 NOT NULL COMMENT '主键',
  `username` varchar(32) CHARACTER SET utf8 NOT NULL COMMENT '用户名称',
  `password` varchar(128) CHARACTER SET utf8 NOT NULL COMMENT '用户密码',
  `realname` varchar(255) CHARACTER SET utf8 NOT NULL COMMENT '真实姓名',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('84de82bffad249d995c168298b5ff610', 'Well', 'Well_19900520', '王奥');
INSERT INTO `user` VALUES ('e0287bb995cc4a60b7bedccd37853381', 'Well', 'Well_19900520', '王奥');

-- ----------------------------
-- Table structure for user_info
-- ----------------------------
DROP TABLE IF EXISTS `user_info`;
CREATE TABLE `user_info` (
  `id` varchar(32) CHARACTER SET utf8 NOT NULL COMMENT '主键',
  `user_id` varchar(32) CHARACTER SET utf8 NOT NULL COMMENT 'user表的id',
  `gender` int(2) NOT NULL DEFAULT '1' COMMENT '性别，1-男，2-女',
  `birthday` datetime DEFAULT NULL COMMENT '生日',
  `insert_user` varchar(32) CHARACTER SET utf8 DEFAULT NULL COMMENT '新增人的id',
  `insert_date` datetime DEFAULT NULL COMMENT '新增时间',
  `update_user` varchar(32) CHARACTER SET utf8 DEFAULT NULL COMMENT '修改人的id',
  `update_date` datetime DEFAULT NULL COMMENT '修改时间',
  `enable` int(2) NOT NULL DEFAULT '1' COMMENT '是否可用，0-禁用，1-可用',
  `memo` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user_info
-- ----------------------------
INSERT INTO `user_info` VALUES ('045c350a83cc491abac81d7ed0a7e102', '25', '1', '2018-08-10 15:34:20', null, '2018-08-10 15:34:20', null, '2018-08-15 11:00:47', '1', null);
INSERT INTO `user_info` VALUES ('05e4f8e3d93948b08b56893f20c7d13b', '46', '1', '2018-08-10 15:34:21', null, '2018-08-10 15:34:21', null, '2018-08-15 11:00:47', '1', null);
INSERT INTO `user_info` VALUES ('0610d06ae0a84d17a3f30c66e6c1ce90', '82', '1', '2018-08-10 15:34:22', null, '2018-08-10 15:34:22', null, '2018-08-15 11:00:47', '1', null);
INSERT INTO `user_info` VALUES ('08cc7f2a09bc42c2b2eff6264e73d44d', '18', '1', '2018-08-10 15:34:19', null, '2018-08-10 15:34:19', null, '2018-08-10 15:34:19', '1', null);
INSERT INTO `user_info` VALUES ('0b965772d6c8408abc05f10deb5026fa', '19', '1', '2018-08-10 15:34:19', null, '2018-08-10 15:34:19', null, '2018-08-10 15:34:19', '1', null);
INSERT INTO `user_info` VALUES ('0ff5236598a3412ba45d68ff3794c6ea', '88', '1', '2018-08-10 15:34:22', null, '2018-08-10 15:34:22', null, '2018-08-10 15:34:22', '1', null);
INSERT INTO `user_info` VALUES ('1390265bd00b42268fadf7a6842c35f1', '72', '1', '2018-08-10 15:34:22', null, '2018-08-10 15:34:22', null, '2018-08-10 15:34:22', '1', null);
INSERT INTO `user_info` VALUES ('147a002ff43242c4ae1c3c46c3fec4f6', '38', '1', '2018-08-10 15:34:20', null, '2018-08-10 15:34:20', null, '2018-08-10 15:34:20', '1', null);
INSERT INTO `user_info` VALUES ('15903737bebf469ba333ac5eb3bd5782', '41', '1', '2018-08-10 15:34:20', null, '2018-08-10 15:34:20', null, '2018-08-10 15:34:20', '1', null);
INSERT INTO `user_info` VALUES ('18a1d824960d4703af4289659a7d3678', '53', '1', '2018-08-10 15:34:21', null, '2018-08-10 15:34:21', null, '2018-08-10 15:34:21', '1', null);
INSERT INTO `user_info` VALUES ('18fa5cfe10644b4dbc7673393eef5ef6', '9', '1', '2018-08-10 15:34:19', null, '2018-08-10 15:34:19', null, '2018-08-10 15:34:19', '1', null);
INSERT INTO `user_info` VALUES ('1bdb884f676f44be9696cea0c708f645', '94', '1', '2018-08-10 15:34:22', null, '2018-08-10 15:34:22', null, '2018-08-10 15:34:22', '1', null);
INSERT INTO `user_info` VALUES ('21ecabc970ff4de19078fe31253fc96d', '23', '1', '2018-08-10 15:34:20', null, '2018-08-10 15:34:20', null, '2018-08-10 15:34:20', '1', null);
INSERT INTO `user_info` VALUES ('228f24c789e04756a252cc1dd145367c', '44', '1', '2018-08-10 15:34:20', null, '2018-08-10 15:34:20', null, '2018-08-10 15:34:20', '1', null);
INSERT INTO `user_info` VALUES ('23db39d99a6e45e9812c4fee7801e3cf', '64', '1', '2018-08-10 15:34:21', null, '2018-08-10 15:34:21', null, '2018-08-10 15:34:21', '1', null);
INSERT INTO `user_info` VALUES ('24653153332347cdbe1f583c3e645b52', '59', '1', '2018-08-10 15:34:21', null, '2018-08-10 15:34:21', null, '2018-08-10 15:34:21', '1', null);
INSERT INTO `user_info` VALUES ('269bf169b6ee40399b5f593da05a846a', '61', '1', '2018-08-10 15:34:21', null, '2018-08-10 15:34:21', null, '2018-08-10 15:34:21', '1', null);
INSERT INTO `user_info` VALUES ('27e8d3afb7d9440ca97c3f76282dd800', '81', '1', '2018-08-10 15:34:22', null, '2018-08-10 15:34:22', null, '2018-08-10 15:34:22', '1', null);
INSERT INTO `user_info` VALUES ('28859ae14fac44769817406563184221', '52', '1', '2018-08-10 15:34:21', null, '2018-08-10 15:34:21', null, '2018-08-10 15:34:21', '1', null);
INSERT INTO `user_info` VALUES ('28e177ddb1ae44fd9588af0d5f498d50', '84', '1', '2018-08-10 15:34:22', null, '2018-08-10 15:34:22', null, '2018-08-10 15:34:22', '1', null);
INSERT INTO `user_info` VALUES ('2a39d4b4b04c40f5b4281ad30c09a150', '56', '1', '2018-08-10 15:34:21', null, '2018-08-10 15:34:21', null, '2018-08-10 15:34:21', '1', null);
INSERT INTO `user_info` VALUES ('32974dffd58d406eaea52b03a4028c00', '15', '1', '2018-08-10 15:34:19', null, '2018-08-10 15:34:19', null, '2018-08-10 15:34:19', '1', null);
INSERT INTO `user_info` VALUES ('37eda0c3684f47cdadb8a0e851ae7874', '68', '1', '2018-08-10 15:34:22', null, '2018-08-10 15:34:22', null, '2018-08-10 15:34:22', '1', null);
INSERT INTO `user_info` VALUES ('407482a85d23446da20880fca8840192', '89', '1', '2018-08-10 15:34:22', null, '2018-08-10 15:34:22', null, '2018-08-10 15:34:22', '1', null);
INSERT INTO `user_info` VALUES ('45009f1119d246618bcb486fb212a843', '36', '1', '2018-08-10 15:34:20', null, '2018-08-10 15:34:20', null, '2018-08-10 15:34:20', '1', null);
INSERT INTO `user_info` VALUES ('4932acad0e564f2b8d9f6f2d1dbc0a48', '97', '1', '2018-08-10 15:34:22', null, '2018-08-10 15:34:22', null, '2018-08-10 15:34:22', '1', null);
INSERT INTO `user_info` VALUES ('52842dc06b034dff8597a590ecde1a33', '77', '1', '2018-08-10 15:34:22', null, '2018-08-10 15:34:22', null, '2018-08-10 15:34:22', '1', null);
INSERT INTO `user_info` VALUES ('528d940956d342658c47303035e78062', '32', '1', '2018-08-10 15:34:20', null, '2018-08-10 15:34:20', null, '2018-08-10 15:34:20', '1', null);
INSERT INTO `user_info` VALUES ('553fad58ea374a0db3598700a6d02214', '87', '1', '2018-08-10 15:34:22', null, '2018-08-10 15:34:22', null, '2018-08-10 15:34:22', '1', null);
INSERT INTO `user_info` VALUES ('57c67a3b0c544ebf8408399611d823c7', '21', '1', '2018-08-10 15:34:19', null, '2018-08-10 15:34:19', null, '2018-08-10 15:34:19', '1', null);
INSERT INTO `user_info` VALUES ('599fea0be6a14ff8a972a65c4507d462', '17', '1', '2018-08-10 15:34:19', null, '2018-08-10 15:34:19', null, '2018-08-10 15:34:19', '1', null);
INSERT INTO `user_info` VALUES ('5a808a191e214bde8e8d15809917349e', '13', '1', '2018-08-10 15:34:19', null, '2018-08-10 15:34:19', null, '2018-08-10 15:34:19', '1', null);
INSERT INTO `user_info` VALUES ('5c73bd03eeb24e7597b2dd1bb1f9195d', '78', '1', '2018-08-10 15:34:22', null, '2018-08-10 15:34:22', null, '2018-08-10 15:34:22', '1', null);
INSERT INTO `user_info` VALUES ('5c91fab8b33849718e8839ea9a580f43', '65', '1', '2018-08-10 15:34:21', null, '2018-08-10 15:34:21', null, '2018-08-10 15:34:21', '1', null);
INSERT INTO `user_info` VALUES ('5d4ca878a51b49a4aacedd6b41606991', '45', '1', '2018-08-10 15:34:20', null, '2018-08-10 15:34:20', null, '2018-08-10 15:34:20', '1', null);
INSERT INTO `user_info` VALUES ('5da4ebd86f3b46878ab5de058f2c8d0b', '86', '1', '2018-08-10 15:34:22', null, '2018-08-10 15:34:22', null, '2018-08-10 15:34:22', '1', null);
INSERT INTO `user_info` VALUES ('5e5cbc0938254b24ba570f816589c532', '49', '1', '2018-08-10 15:34:21', null, '2018-08-10 15:34:21', null, '2018-08-10 15:34:21', '1', null);
INSERT INTO `user_info` VALUES ('6626c2735e6045e49e45c559f8568086', '48', '1', '2018-08-10 15:34:21', null, '2018-08-10 15:34:21', null, '2018-08-10 15:34:21', '1', null);
INSERT INTO `user_info` VALUES ('66a2c7d6941348e48edbac6a8e5459d6', '99', '1', '2018-08-10 15:34:22', null, '2018-08-10 15:34:22', null, '2018-08-10 15:34:22', '1', null);
INSERT INTO `user_info` VALUES ('69ec1180551746ddb24ea6b4d00ed79a', '31', '1', '2018-08-10 15:34:20', null, '2018-08-10 15:34:20', null, '2018-08-10 15:34:20', '1', null);
INSERT INTO `user_info` VALUES ('6b534b71a6d843b5a7377fba56f33226', '3', '1', '2018-08-10 15:34:19', null, '2018-08-10 15:34:19', null, '2018-08-10 15:34:19', '1', null);
INSERT INTO `user_info` VALUES ('6be5fe73f8cd499ea117f2a3a13db9f2', '58', '1', '2018-08-10 15:34:21', null, '2018-08-10 15:34:21', null, '2018-08-10 15:34:21', '1', null);
INSERT INTO `user_info` VALUES ('6ea84a4c85f443fe9963881c6c661fe8', '79', '1', '2018-08-10 15:34:22', null, '2018-08-10 15:34:22', null, '2018-08-10 15:34:22', '1', null);
INSERT INTO `user_info` VALUES ('710f101ed4de41ba9260833a6b218864', '1', '1', '2018-08-10 15:34:18', null, '2018-08-10 15:34:18', null, '2018-08-10 15:34:18', '1', null);
INSERT INTO `user_info` VALUES ('7141896034db4e2aaf469a3b108736b0', '5', '1', '2018-08-10 15:34:19', null, '2018-08-10 15:34:19', null, '2018-08-10 15:34:19', '1', null);
INSERT INTO `user_info` VALUES ('72ebc5d128054515b6987e39ff196d3d', '14', '1', '2018-08-10 15:34:19', null, '2018-08-10 15:34:19', null, '2018-08-10 15:34:19', '1', null);
INSERT INTO `user_info` VALUES ('7c89b80088ad4e9884abc3afd84a4704', '50', '1', '2018-08-10 15:34:21', null, '2018-08-10 15:34:21', null, '2018-08-10 15:34:21', '1', null);
INSERT INTO `user_info` VALUES ('7ca0e86752db41fc80558656bb9b0c62', '66', '1', '2018-08-10 15:34:22', null, '2018-08-10 15:34:22', null, '2018-08-10 15:34:22', '1', null);
INSERT INTO `user_info` VALUES ('7f349ed8802d4e73b242bca2bf9b5e18', '55', '1', '2018-08-10 15:34:21', null, '2018-08-10 15:34:21', null, '2018-08-10 15:34:21', '1', null);
INSERT INTO `user_info` VALUES ('80890c78f39a4cd1b522125ad7565ff3', '93', '1', '2018-08-10 15:34:22', null, '2018-08-10 15:34:22', null, '2018-08-10 15:34:22', '1', null);
INSERT INTO `user_info` VALUES ('811ea9efb50f48a194a5845e5a163793', '8', '1', '2018-08-10 15:34:19', null, '2018-08-10 15:34:19', null, '2018-08-10 15:34:19', '1', null);
INSERT INTO `user_info` VALUES ('81e3acafa5454308a197b5fe57f6305f', '84de82bffad249d995c168298b5ff610', '2', '2018-07-02 17:24:00', null, '2018-07-02 17:24:00', null, '2018-07-02 17:24:00', '1', null);
INSERT INTO `user_info` VALUES ('829c637fe0fe424d8360f062945756f4', '33', '1', '2018-08-10 15:34:20', null, '2018-08-10 15:34:20', null, '2018-08-10 15:34:20', '1', null);
INSERT INTO `user_info` VALUES ('83b4d64d373646c38812559879f7cf69', '42', '1', '2018-08-10 15:34:20', null, '2018-08-10 15:34:20', null, '2018-08-10 15:34:20', '1', null);
INSERT INTO `user_info` VALUES ('840564edaaea437bae42ba5e83cf3cb9', '83', '1', '2018-08-10 15:34:22', null, '2018-08-10 15:34:22', null, '2018-08-10 15:34:22', '1', null);
INSERT INTO `user_info` VALUES ('868fc667012940a5967199f3de2c786e', '95', '1', '2018-08-10 15:34:22', null, '2018-08-10 15:34:22', null, '2018-08-10 15:34:22', '1', null);
INSERT INTO `user_info` VALUES ('86f6ba6cd8dd430b804f863ee584ff0c', '12', '1', '2018-08-10 15:34:19', null, '2018-08-10 15:34:19', null, '2018-08-10 15:34:19', '1', null);
INSERT INTO `user_info` VALUES ('877006d857dd45b39099cc26c811c344', '39', '1', '2018-08-10 15:34:20', null, '2018-08-10 15:34:20', null, '2018-08-10 15:34:20', '1', null);
INSERT INTO `user_info` VALUES ('88437c288b3144739bbf31eac5a50f8a', '92', '1', '2018-08-10 15:34:22', null, '2018-08-10 15:34:22', null, '2018-08-10 15:34:22', '1', null);
INSERT INTO `user_info` VALUES ('9081c7e5b5da4252ad28b395bded1154', '40', '1', '2018-08-10 15:34:20', null, '2018-08-10 15:34:20', null, '2018-08-10 15:34:20', '1', null);
INSERT INTO `user_info` VALUES ('9453e645076a4b36a81361618fae26fc', '37', '1', '2018-08-10 15:34:20', null, '2018-08-10 15:34:20', null, '2018-08-10 15:34:20', '1', null);
INSERT INTO `user_info` VALUES ('a057ece7193d48dd8ace457c3a87a076', '11', '1', '2018-08-10 15:34:19', null, '2018-08-10 15:34:19', null, '2018-08-10 15:34:19', '1', null);
INSERT INTO `user_info` VALUES ('a6c8abee4e8a42b6bf0b25a8ff4d30d3', '62', '1', '2018-08-10 15:34:21', null, '2018-08-10 15:34:21', null, '2018-08-10 15:34:21', '1', null);
INSERT INTO `user_info` VALUES ('ab22ccf771c349cfa72ec9c8def7002e', '75', '1', '2018-08-10 15:34:22', null, '2018-08-10 15:34:22', null, '2018-08-10 15:34:22', '1', null);
INSERT INTO `user_info` VALUES ('ae512d46853044749a813e743b33397f', '74', '1', '2018-08-10 15:34:22', null, '2018-08-10 15:34:22', null, '2018-08-10 15:34:22', '1', null);
INSERT INTO `user_info` VALUES ('afdc0b63702f4313b43f7f1b496ee4bd', '28', '1', '2018-08-10 15:34:20', null, '2018-08-10 15:34:20', null, '2018-08-10 15:34:20', '1', null);
INSERT INTO `user_info` VALUES ('affec0ba12db4ecea1985efa9fc54a18', '16', '1', '2018-08-10 15:34:19', null, '2018-08-10 15:34:19', null, '2018-08-10 15:34:19', '1', null);
INSERT INTO `user_info` VALUES ('b05c33bd28664e8f848088487841c6e6', '98', '1', '2018-08-10 15:34:22', null, '2018-08-10 15:34:22', null, '2018-08-10 15:34:22', '1', null);
INSERT INTO `user_info` VALUES ('b06eea08f28d429bb580b1d9c449c727', '10', '1', '2018-08-10 15:34:19', null, '2018-08-10 15:34:19', null, '2018-08-10 15:34:19', '1', null);
INSERT INTO `user_info` VALUES ('b0b446b069c34f81bf542db8b54e11fe', '29', '1', '2018-08-10 15:34:20', null, '2018-08-10 15:34:20', null, '2018-08-10 15:34:20', '1', null);
INSERT INTO `user_info` VALUES ('b5259149d16d4bd9b838fa6485a5531b', '35', '1', '2018-08-10 15:34:20', null, '2018-08-10 15:34:20', null, '2018-08-10 15:34:20', '1', null);
INSERT INTO `user_info` VALUES ('b75d2b5cb198464790c993d37518dfcf', '100', '1', '2018-08-10 15:34:22', null, '2018-08-10 15:34:22', null, '2018-08-10 15:34:22', '1', null);
INSERT INTO `user_info` VALUES ('b9554cdc1ed9457d810af2b695d357b4', '30', '1', '2018-08-10 15:34:20', null, '2018-08-10 15:34:20', null, '2018-08-10 15:34:20', '1', null);
INSERT INTO `user_info` VALUES ('ba2017f4e73e4a6badd6bb19d440c78a', '67', '1', '2018-08-10 15:34:22', null, '2018-08-10 15:34:22', null, '2018-08-10 15:34:22', '1', null);
INSERT INTO `user_info` VALUES ('ba792bffcd7941d4b75abab83a332b0b', '70', '1', '2018-08-10 15:34:22', null, '2018-08-10 15:34:22', null, '2018-08-10 15:34:22', '1', null);
INSERT INTO `user_info` VALUES ('bd27885004ab4d9a9278787e2256cbcd', '47', '1', '2018-08-10 15:34:21', null, '2018-08-10 15:34:21', null, '2018-08-10 15:34:21', '1', null);
INSERT INTO `user_info` VALUES ('be78e889fac748e1991cc7e315412662', '91', '1', '2018-08-10 15:34:22', null, '2018-08-10 15:34:22', null, '2018-08-10 15:34:22', '1', null);
INSERT INTO `user_info` VALUES ('c2b720eb4516414e968bc0a45f601855', '90', '1', '2018-08-10 15:34:22', null, '2018-08-10 15:34:22', null, '2018-08-10 15:34:22', '1', null);
INSERT INTO `user_info` VALUES ('c65adf6916f44230be02e30ee041a16a', '26', '1', '2018-08-10 15:34:20', null, '2018-08-10 15:34:20', null, '2018-08-10 15:34:20', '1', null);
INSERT INTO `user_info` VALUES ('c9cc57fb41f146fda0c1a31f15df1a68', '22', '1', '2018-08-10 15:34:20', null, '2018-08-10 15:34:20', null, '2018-08-10 15:34:20', '1', null);
INSERT INTO `user_info` VALUES ('ccb73ee1c49b45dda70f3bd154fd7313', '71', '1', '2018-08-10 15:34:22', null, '2018-08-10 15:34:22', null, '2018-08-10 15:34:22', '1', null);
INSERT INTO `user_info` VALUES ('cfa34b1524c04200900f08a4e1004af6', '51', '1', '2018-08-10 15:34:21', null, '2018-08-10 15:34:21', null, '2018-08-10 15:34:21', '1', null);
INSERT INTO `user_info` VALUES ('d3435881a7d14f028a3975d9a080bfbd', '2', '1', '2018-08-10 15:34:19', null, '2018-08-10 15:34:19', null, '2018-08-10 15:34:19', '1', null);
INSERT INTO `user_info` VALUES ('d78c273ed1b34d84817825e28c01f1ad', '69', '1', '2018-08-10 15:34:22', null, '2018-08-10 15:34:22', null, '2018-08-10 15:34:22', '1', null);
INSERT INTO `user_info` VALUES ('d964c42b344b40689dc06ae5977028e5', '60', '1', '2018-08-10 15:34:21', null, '2018-08-10 15:34:21', null, '2018-08-10 15:34:21', '1', null);
INSERT INTO `user_info` VALUES ('d9bfbc26a7f246b29d1ac550c2792e47', '27', '1', '2018-08-10 15:34:20', null, '2018-08-10 15:34:20', null, '2018-08-10 15:34:20', '1', null);
INSERT INTO `user_info` VALUES ('dcebe676d96a455c8c3ccaedd763c938', '34', '1', '2018-08-10 15:34:20', null, '2018-08-10 15:34:20', null, '2018-08-10 15:34:20', '1', null);
INSERT INTO `user_info` VALUES ('dceecf90f96948919423cd2c152701ab', '6', '1', '2018-08-10 15:34:19', null, '2018-08-10 15:34:19', null, '2018-08-10 15:34:19', '1', null);
INSERT INTO `user_info` VALUES ('e160f5a7677046b0b8a2266f2f887805', '43', '1', '2018-08-10 15:34:20', null, '2018-08-10 15:34:20', null, '2018-08-10 15:34:20', '1', null);
INSERT INTO `user_info` VALUES ('e22d447860a644719d0fe58c241f24c5', '80', '1', '2018-08-10 15:34:22', null, '2018-08-10 15:34:22', null, '2018-08-10 15:34:22', '1', null);
INSERT INTO `user_info` VALUES ('e2d203be42124a69895bb3f70d4fcac7', '54', '1', '2018-08-10 15:34:21', null, '2018-08-10 15:34:21', null, '2018-08-10 15:34:21', '1', null);
INSERT INTO `user_info` VALUES ('e65c32eee1a04597940954ae8dfa2e96', '96', '1', '2018-08-10 15:34:22', null, '2018-08-10 15:34:22', null, '2018-08-10 15:34:22', '1', null);
INSERT INTO `user_info` VALUES ('ea921a9dbb25480095d87b84f11aa66a', '20', '1', '2018-08-10 15:34:19', null, '2018-08-10 15:34:19', null, '2018-08-10 15:34:19', '1', null);
INSERT INTO `user_info` VALUES ('eb59b91a92f74e5c8a1dcc96ebcb4886', '76', '1', '2018-08-10 15:34:22', null, '2018-08-10 15:34:22', null, '2018-08-10 15:34:22', '1', null);
INSERT INTO `user_info` VALUES ('ec609051ece643b0b8d150346ea71292', '7', '1', '2018-08-10 15:34:19', null, '2018-08-10 15:34:19', null, '2018-08-10 15:34:19', '1', null);
INSERT INTO `user_info` VALUES ('ee2b849ffedc4f0a897d37e238cbff98', '63', '1', '2018-08-10 15:34:21', null, '2018-08-10 15:34:21', null, '2018-08-10 15:34:21', '1', null);
INSERT INTO `user_info` VALUES ('f15bfc757a594d6b8cd8dfd374c49a68', '85', '1', '2018-08-10 15:34:22', null, '2018-08-10 15:34:22', null, '2018-08-10 15:34:22', '1', null);
INSERT INTO `user_info` VALUES ('f61d23b76227498ea57c6c701e974479', '73', '1', '2018-08-10 15:34:22', null, '2018-08-10 15:34:22', null, '2018-08-10 15:34:22', '1', null);
INSERT INTO `user_info` VALUES ('f6dd78372afe42a48c6fd5d3d9b23b7a', '57', '1', '2018-08-10 15:34:21', null, '2018-08-10 15:34:21', null, '2018-08-10 15:34:21', '1', null);
INSERT INTO `user_info` VALUES ('f95ef8d7006b4d5b9d465411ade8bfcb', '24', '1', '2018-08-10 15:34:20', null, '2018-08-10 15:34:20', null, '2018-08-10 15:34:20', '1', null);
INSERT INTO `user_info` VALUES ('fc0418a036ac4985a88effd77b8da8e8', 'e0287bb995cc4a60b7bedccd37853381', '1', '1990-05-20 00:00:00', null, '2018-07-02 17:24:01', null, '2018-07-02 17:24:01', '1', null);
INSERT INTO `user_info` VALUES ('ff49c5f4c0bb4acb9dcef7ed9e522fae', '4', '1', '2018-08-10 15:34:19', null, '2018-08-10 15:34:19', null, '2018-08-10 15:34:19', '1', null);
