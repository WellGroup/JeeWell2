package com.well.jeewell.module.user.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.well.jeewell.module.user.entity.User;
import com.well.jeewell.module.user.mapper.UserMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.List;

/**
 * Copyright &copy; Well All rights reserved.
 * Author:Well
 * Date:2019-03-28
 * Description:
 */

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTest {

    @Autowired
    private UserService userService;

    @Test
    public void testInsert(){
        User user=new User();
        user.setUsername("zangzhaobin");
        user.setRealname("臧兆彬");
        user.setPassword("111111");
        int i=userService.insert(user);
        if(i>0){
            System.out.println("操作成功！user id 为："+user.getId());
        }else{
            System.out.println("操作失败！");
        }
    }

    @Test
    public void testSelectList(){
        User user=new User();
        user.setUsername("11");
        List<User> userList=userService.selectList(user);
        if(null==userList || userList.size()==0){
            System.out.println("未查询到数据！");
        }else{
            System.out.println(userList.get(0));
        }
    }

    @Test
    public void testSelectAll(){
        int pageNum=1;
        int pageSize=2;
        PageHelper.startPage(pageNum,pageSize);
        List<User> userList=userService.selectAll();
        PageInfo<User> pageInfo=new PageInfo<>(userList);
        System.out.println(pageInfo);
    }

}
