package com.well.jeewell.core.util;

import org.junit.Test;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class NumberUtilTest {

    @Test
    public void testFormat(){
        int i=12345678;
        String s=NumberUtil.format(i);
        System.out.println(s);
        long l=123456789L;
        s=NumberUtil.format(l);
        System.out.println(s);
        double d=123456.789;
        s=NumberUtil.format(d);
        System.out.println(s);
        s=NumberUtil.format(d,1);
        System.out.println(s);
        s=NumberUtil.format(d,3);
        System.out.println(s);
        d=12.345;
        s=NumberUtil.format(d);
        System.out.println(s);
        d=12.544;
        s=NumberUtil.format(d);
        System.out.println(s);
        d=12.545;
        s=NumberUtil.format(d);
        System.out.println(s);
        d=12.644;
        s=NumberUtil.format(d);
        System.out.println(s);
        d=12.645;
        s=NumberUtil.format(d);
        System.out.println(s);
        d=29.994;
        s=NumberUtil.format(d);
        System.out.println(s);
    }

    @Test
    public void testFormatNumber(){
        int i=12345678;
        String s=NumberUtil.formatNumber(i);
        System.out.println(s);
        long l=123456789L;
        s=NumberUtil.formatNumber(l);
        System.out.println(s);
        double d=12345.6789;
        s=NumberUtil.formatNumber(d);
        System.out.println(s);
        s=NumberUtil.formatNumber(d,1);
        System.out.println(s);
        s=NumberUtil.formatNumber(d,3);
        System.out.println(s);
        d=0.123;
        s=NumberUtil.formatNumber(d);
        System.out.println(s);
        d=0.125;
        s=NumberUtil.formatNumber(d);
        System.out.println(s);
    }

    @Test
    public void testFormatInteger(){
        int i=12345678;
        String s=NumberUtil.formatInteger(i);
        System.out.println(s);
        long l=123456789L;
        s=NumberUtil.formatInteger(l);
        System.out.println(s);
        double d=12345.6789;
        s=NumberUtil.formatInteger(d);
        System.out.println(s);
        s=NumberUtil.formatInteger(d,1);
        System.out.println(s);
        s=NumberUtil.formatInteger(d,3);
        System.out.println(s);
        d=0.123;
        s=NumberUtil.formatInteger(d);
        System.out.println(s);
        d=0.125;
        s=NumberUtil.formatInteger(d);
        System.out.println(s);
    }

    @Test
    public void testFormatPercent(){
        int i=12345678;
        String s=NumberUtil.formatPercent(i);
        System.out.println(s);
        s=NumberUtil.formatPercent(i,1);
        System.out.println(s);
        s=NumberUtil.formatPercent(i,2);
        System.out.println(s);
        long l=123456789L;
        s=NumberUtil.formatPercent(l);
        System.out.println(s);
        double d=123456.789;
        s=NumberUtil.formatPercent(d);
        System.out.println(s);
        s=NumberUtil.formatPercent(d,1);
        System.out.println(s);
        s=NumberUtil.formatPercent(d,3);
        System.out.println(s);
        d=123456789;
        s=NumberUtil.formatPercent(d);
        System.out.println(s);
        d=0.12344;
        s=NumberUtil.formatPercent(d);
        System.out.println(s);
        d=0.12544;
        s=NumberUtil.formatPercent(d);
        System.out.println(s);
        d=0.12545;
        s=NumberUtil.formatPercent(d);
        System.out.println(s);
        d=0.12945;
        s=NumberUtil.formatPercent(d);
        System.out.println(s);
    }

    @Test
    public void testFormatCurrency(){
        int i=12345678;
        String s=NumberUtil.formatCurrency(i);
        System.out.println(s);
        s=NumberUtil.formatCurrency(i,0);
        System.out.println(s);
        s=NumberUtil.formatCurrency(i,1);
        System.out.println(s);
        long l=123456789L;
        s=NumberUtil.formatCurrency(l);
        System.out.println(s);
        s=NumberUtil.formatCurrency(l,0);
        System.out.println(s);
        s=NumberUtil.formatCurrency(l,-2);
        System.out.println(s);
        s=NumberUtil.formatCurrency(l,3);
        System.out.println(s);
        double d=12345.6789;
        s=NumberUtil.formatCurrency(d);
        System.out.println(s);
        s=NumberUtil.formatCurrency(d,1);
        System.out.println(s);
        s=NumberUtil.formatCurrency(d,3);
        System.out.println(s);
        d=12345678.9;
        s=NumberUtil.formatCurrency(d);
        System.out.println(s);
        d=0.123;
        s=NumberUtil.formatCurrency(d);
        System.out.println(s);
        d=0.125;
        s=NumberUtil.formatCurrency(d);
        System.out.println(s);
    }

}
