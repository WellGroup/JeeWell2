package com.well.jeewell.core.util;

import org.junit.Test;

/**
 * Copyright &copy; Well All rights reserved.
 * Author:Well
 * Date:2019-03-25
 * Description:
 */

//@SpringBootTest
public class IDCardUtilTest {

    String idCard="320123199005201430";

    @Test
    public void testGetBirthByIdCard(){
        String s=IDCardUtil.getBirthDay(idCard);
        System.out.println(s);
    }

    @Test
    public void testGetAgeByIdCard(){
        int age=IDCardUtil.getAge(idCard);
        System.out.println(age);
    }

}
