package com.well.jeewell.core.mybatis.generator.config;

import org.apache.commons.lang3.StringUtils;
import org.mybatis.generator.config.PluginConfiguration;

/**
 * Copyright &copy; Well All rights reserved.
 * Author:Well
 * Date:2018-09-03
 * Description:通用 Mapper 的插件配置
 */

public class MapperPluginConfiguration extends PluginConfiguration{

    private String configurationType="tk.mybatis.mapper.generator.MapperPlugin";
    private String mappers="com.well.kernel.mybatis.mapper.ABaseMapper";
    private String caseSensitive="true";
    private String forceAnnotation="true";
    private String beginningDelimiter="";
    private String endingDelimiter="";

    public MapperPluginConfiguration(){
        super.setConfigurationType(this.configurationType);
        super.addProperty("mappers",this.mappers);
        super.addProperty("caseSensitive",this.caseSensitive);
        super.addProperty("forceAnnotation",this.forceAnnotation);
        super.addProperty("beginningDelimiter",beginningDelimiter);
        super.addProperty("endingDelimiter",endingDelimiter);
    }

    public void setMappers(String mappers){
        super.addProperty("mappers",mappers);
    }

    public void setCaseSensitive(String caseSensitive){
        super.addProperty("caseSensitive",caseSensitive);
    }

    public void setForceAnnotation(String forceAnnotation){
        super.addProperty("forceAnnotation",forceAnnotation);
    }

    public void setBeginningDelimiter(String beginningDelimiter) {
        super.addProperty("beginningDelimiter",beginningDelimiter);
    }

    public void setEndingDelimiter(String endingDelimiter) {
        super.addProperty("endingDelimiter",endingDelimiter);
    }

}
