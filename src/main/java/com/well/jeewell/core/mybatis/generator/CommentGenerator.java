package com.well.jeewell.core.mybatis.generator;

import com.well.jeewell.core.util.DateUtil;
import org.mybatis.generator.api.IntrospectedColumn;
import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.dom.java.Field;
import org.mybatis.generator.api.dom.java.FullyQualifiedJavaType;
import org.mybatis.generator.api.dom.java.Method;
import org.mybatis.generator.api.dom.java.TopLevelClass;
import org.mybatis.generator.api.dom.xml.TextElement;
import org.mybatis.generator.api.dom.xml.XmlElement;
import org.mybatis.generator.internal.DefaultCommentGenerator;

import java.util.Date;
import java.util.Set;

/**
 * Copyright &copy; Well All rights reserved.
 * Author:Well
 * Date:2018-08-28
 * Description:
 */

public class CommentGenerator extends DefaultCommentGenerator{

    /**
     * 增加实体类的注释
     * @param topLevelClass
     * @param introspectedTable
     */
    @Override
    public void addModelClassComment(TopLevelClass topLevelClass, IntrospectedTable introspectedTable) {
        topLevelClass.addJavaDocLine("/**");
        topLevelClass.addJavaDocLine(" * Copyright &copy; Well All rights reserved.");
        topLevelClass.addJavaDocLine(" * Author:");
        topLevelClass.addJavaDocLine(" * Date:"+ DateUtil.format(new Date(),"yyyy-MM-dd"));
        topLevelClass.addJavaDocLine(" * Description:");
        topLevelClass.addJavaDocLine(" */");
        topLevelClass.addJavaDocLine("");
    }

    /**
     * 增加实体类中的字段注释
     * @param field
     * @param introspectedTable
     * @param introspectedColumn
     */
    @Override
    public void addFieldComment(Field field, IntrospectedTable introspectedTable, IntrospectedColumn introspectedColumn) {
        field.addJavaDocLine("/* " + introspectedColumn.getRemarks() + " */");
        StringBuilder sb=new StringBuilder();
        sb.append("@Column(");
        sb.append("name=\"").append(introspectedColumn.getActualColumnName()).append("\"");
        sb.append(",columnDefinition=\"").append(introspectedColumn.getRemarks()).append("\"");
        FullyQualifiedJavaType fullyQualifiedJavaType = introspectedColumn.getFullyQualifiedJavaType();
        if(!"Date".equalsIgnoreCase(fullyQualifiedJavaType.getShortName()))
        {
            sb.append(",length=").append(introspectedColumn.getLength());
        }
        sb.append(",nullable=").append(introspectedColumn.isNullable());
        sb.append(")");
        field.addAnnotation(sb.toString());
    }

    /**
     * 增加实体类中的 setter 方法注释
     * @param method
     * @param introspectedTable
     * @param introspectedColumn
     */
    @Override
    public void addGetterComment(Method method, IntrospectedTable introspectedTable, IntrospectedColumn introspectedColumn) {
        method.addJavaDocLine("/**");
        method.addJavaDocLine(" * 获取 "+introspectedColumn.getRemarks());
        method.addJavaDocLine(" * @return " + introspectedColumn.getJavaProperty() + " " + introspectedColumn.getRemarks());
        method.addJavaDocLine(" */");
    }

    /**
     * 增加实体类中的 getter 方法注释
     * @param method
     * @param introspectedTable
     * @param introspectedColumn
     */
    @Override
    public void addSetterComment(Method method, IntrospectedTable introspectedTable, IntrospectedColumn introspectedColumn) {
        method.addJavaDocLine("/**");
        method.addJavaDocLine(" * 设置 "+introspectedColumn.getRemarks());
        method.addJavaDocLine(" * @param " + introspectedColumn.getJavaProperty() + " " + introspectedColumn.getRemarks());
        method.addJavaDocLine(" */");
    }

    @Override
    public void addComment(XmlElement xmlElement) {
//        xmlElement.addElement(new TextElement("<!--"));
//        xmlElement.addElement(new TextElement(" Copyright &copy; Well All rights reserved."));
//        xmlElement.addElement(new TextElement(" Author:"));
//        xmlElement.addElement(new TextElement(" Date:"+ DateUtil.format(new Date(),"yyyy-MM-dd")));
//        xmlElement.addElement(new TextElement(" Description:"));
//        xmlElement.addElement(new TextElement("-->"));
    }
}
