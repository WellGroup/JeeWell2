package com.well.jeewell.core.mybatis.generator.config;

/**
 * Copyright &copy; Well All rights reserved.
 * Author:Well
 * Date:2018-09-04
 * Description:mapper 的模板文件插件配置
 */

public class MapperTemplateFilePluginConfiguration extends TemplateFilePluginConfiguratrion{

    private String templatePath="template/ftl/mapper.java.ftl";
    private String fileName="${tableClass.shortClassName}Mapper.java";

    public MapperTemplateFilePluginConfiguration(){
        super.setTemplatePath(this.templatePath);
        super.setFileName(this.fileName);
    }

}
