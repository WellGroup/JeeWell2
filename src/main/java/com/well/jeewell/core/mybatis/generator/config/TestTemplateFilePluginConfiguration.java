package com.well.jeewell.core.mybatis.generator.config;

/**
 * Copyright &copy; Well All rights reserved.
 * User: Well
 * Date:2018-09-06
 * Description:test 模板文件插件配置
 */

public class TestTemplateFilePluginConfiguration extends TemplateFilePluginConfiguratrion{

    private String templatePath="template/ftl/test.java.ftl";
    private String fileName="${tableClass.shortClassName}ServiceTest.java";

    public TestTemplateFilePluginConfiguration(){
        super.setTemplatePath(this.templatePath);
        super.setFileName(this.fileName);
    }

}
