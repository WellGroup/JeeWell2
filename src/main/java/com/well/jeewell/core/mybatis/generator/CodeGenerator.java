package com.well.jeewell.core.mybatis.generator;

import com.well.jeewell.core.mybatis.generator.config.*;
import lombok.extern.log4j.Log4j2;
import org.mybatis.generator.api.GeneratedJavaFile;
import org.mybatis.generator.api.GeneratedXmlFile;
import org.mybatis.generator.api.MyBatisGenerator;
import org.mybatis.generator.config.*;
import org.mybatis.generator.internal.DefaultShellCallback;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Copyright &copy; Well All rights reserved.
 * Author:Well
 * Date:2018-09-03
 * Description:代码生成器，生成 controller、test、service、mapper、xml、entity、index、form 文件
 * 通过运行 main 方法生成文件
 */

@Log4j2
public class CodeGenerator {

    private List<String> warningList = new ArrayList<String>(); // 警告列表
    private boolean overwrite = true; // 覆盖已有文件
    private Configuration configuration = new Configuration(); // 代码生成器的配置
    private Context context=new Context(ModelType.FLAT); // 代码生成器的上下文环境

    public CodeGenerator(){
        context.setId("context");
        context.setTargetRuntime("MyBatis3Simple");
        context.addProperty("javaFileEncoding","UTF-8");
        context.addProperty("useMapperCommentGenerator","false");
    }

    /**
     * 生成方法
     */
    public void generate(){
        try{
            /* Mybatis Mapper 的插件配置 */
            MapperPluginConfiguration mapperPluginConfiguration=new MapperPluginConfiguration();
            context.addPluginConfiguration(mapperPluginConfiguration);
            /* controller 模板插件配置 */
            ControllerTemplateFilePluginConfiguration controllerTemplateFilePluginConfiguratrion=new ControllerTemplateFilePluginConfiguration();
            controllerTemplateFilePluginConfiguratrion.setTargetPackage("com.well.jeewell.module.user.web");
            context.addPluginConfiguration(controllerTemplateFilePluginConfiguratrion);
            /* test 模板插件配置 */
            TestTemplateFilePluginConfiguration testTemplateFilePluginConfiguration=new TestTemplateFilePluginConfiguration();
            testTemplateFilePluginConfiguration.setTargetPackage("com.well.jeewell.module.user.service");
            context.addPluginConfiguration(testTemplateFilePluginConfiguration);
            /* service 模板插件配置 */
            ServiceTemplateFilePluginConfiguration serviceTemplateFilePluginConfiguratrion=new ServiceTemplateFilePluginConfiguration();
            serviceTemplateFilePluginConfiguratrion.setTargetPackage("com.well.jeewell.module.user.service");
            context.addPluginConfiguration(serviceTemplateFilePluginConfiguratrion);
            /* mapper 模板插件配置 */
            MapperTemplateFilePluginConfiguration mapperTemplateFilePluginConfiguration=new MapperTemplateFilePluginConfiguration();
            mapperTemplateFilePluginConfiguration.setTargetPackage("com.well.jeewell.module.user.mapper");
            context.addPluginConfiguration(mapperTemplateFilePluginConfiguration);
            /* xml 模板插件配置 */
            XmlTemplateFilePluginConfiguration xmlTemplateFilePluginConfiguration=new XmlTemplateFilePluginConfiguration();
            xmlTemplateFilePluginConfiguration.setTargetPackage("com.well.jeewell.module.user.mapper");
            context.addPluginConfiguration(xmlTemplateFilePluginConfiguration);
            /* TODO:index 模板插件配置 */
            /* TODO:form 模板插件配置 */
            /* 注释生成器配置 */
            CommentGeneratorConfiguration commentGeneratorConfiguration=new CommentGeneratorConfiguration();
            commentGeneratorConfiguration.setConfigurationType("com.well.jeewell.core.mybatis.generator.CommentGenerator");
            context.setCommentGeneratorConfiguration(commentGeneratorConfiguration);
            /* JDBC 连接配置 */
            JDBCConnectionConfiguration jdbcConnectionConfiguration=new JDBCConnectionConfiguration();
            jdbcConnectionConfiguration.setDriverClass("com.mysql.jdbc.Driver");
            jdbcConnectionConfiguration.setConnectionURL("jdbc:MySQL://127.0.0.1:3306/jeewell");
            jdbcConnectionConfiguration.setUserId("root");
            jdbcConnectionConfiguration.setPassword("root");
            context.setJdbcConnectionConfiguration(jdbcConnectionConfiguration);
            /* 生成 entity 类的配置 */
            JavaModelGeneratorConfiguration javaModelGeneratorConfiguration=new JavaModelGeneratorConfiguration();
            javaModelGeneratorConfiguration.setTargetProject("target/output/java");
            javaModelGeneratorConfiguration.setTargetPackage("com.well.jeewell.module.user.entity");
            javaModelGeneratorConfiguration.addProperty("rootClass","com.well.jeewell.core.mybatis.persistence.ABaseEntity");
            context.setJavaModelGeneratorConfiguration(javaModelGeneratorConfiguration);
            /* 表配置 */
            TableConfiguration tableConfiguration=new TableConfiguration(context);
            tableConfiguration.setTableName("user_info");
            context.addTableConfiguration(tableConfiguration);
            /* 添加代码生成器的上下文环境 */
            configuration.addContext(context);
            /* 生成代码 */
            DefaultShellCallback callback = new DefaultShellCallback(overwrite);
            MyBatisGenerator myBatisGenerator = new MyBatisGenerator(configuration, callback, warningList);
            myBatisGenerator.generate(null);
            /* 打印生成结果 */
            this.printResult(myBatisGenerator, warningList);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * 打印生成结果
     * @param myBatisGenerator
     * @param warnings
     */
    private void printResult(MyBatisGenerator myBatisGenerator,List<String> warnings){
        List<GeneratedJavaFile> generatedJavaFileList=myBatisGenerator.getGeneratedJavaFiles();
        Iterator<GeneratedJavaFile> iteratorJava=generatedJavaFileList.iterator();
        GeneratedJavaFile generatedJavaFile=null;
        System.out.println("生成的文件：");
        while(iteratorJava.hasNext()){
            generatedJavaFile=iteratorJava.next();
            System.out.println(" "+generatedJavaFile.getFileName());
        }
        List<GeneratedXmlFile> generatedXmlFileList=myBatisGenerator.getGeneratedXmlFiles();
        Iterator<GeneratedXmlFile> iteratorXml=generatedXmlFileList.iterator();
        GeneratedXmlFile generatedXmlFile=null;
        while(iteratorXml.hasNext()){
            generatedXmlFile=iteratorXml.next();
            System.out.println(" "+generatedXmlFile.getFileName());
        }
        if(warnings.size()!=0){
            System.out.println("警告：");
            for(String warning:warnings){
                System.out.println(" "+warning);
            }
        }
        System.out.println("生成文件成功！");
    }

    /**
     * 生成代码的入口方法
     * @param args
     */
    public static void main(String[] args){
        /* 创建 java 相关代码生成文件所在的目录 */
        File javaTargetProject=new File("target/output/java");
        if(!javaTargetProject.exists()){
            javaTargetProject.mkdirs();
        }
        /* 创建 web 相关代码生成文件所在的目录 */
        File webTargetProject=new File("target/output/web");
        if(!webTargetProject.exists()){
            webTargetProject.mkdirs();
        }
        /* 生成代码文件 */
        CodeGenerator codeGenerator=new CodeGenerator();
        codeGenerator.generate();
    }

}
