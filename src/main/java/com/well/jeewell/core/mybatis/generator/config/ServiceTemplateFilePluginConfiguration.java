package com.well.jeewell.core.mybatis.generator.config;

/**
 * Copyright &copy; Well All rights reserved.
 * Author:Well
 * Date:2018-09-06
 * Description:service 的模板文件插件配置
 */

public class ServiceTemplateFilePluginConfiguration extends TemplateFilePluginConfiguratrion{

    private String templatePath="template/ftl/service.java.ftl";
    private String fileName="${tableClass.shortClassName}Service.java";

    public ServiceTemplateFilePluginConfiguration(){
        super.setTemplatePath(this.templatePath);
        super.setFileName(this.fileName);
    }

}
