package com.well.jeewell.core.mybatis.mapper.provider;

import com.well.jeewell.core.util.DateUtil;
import org.apache.ibatis.mapping.MappedStatement;
import tk.mybatis.mapper.entity.EntityColumn;
import tk.mybatis.mapper.mapperhelper.MapperHelper;
import tk.mybatis.mapper.mapperhelper.MapperTemplate;
import tk.mybatis.mapper.mapperhelper.SqlHelper;

import java.util.Date;

/**
 * Copyright &copy; Well All rights reserved.
 * Author:Well
 * Date:2017-12-04
 * Description:
 */

public class ABaseProvider extends MapperTemplate{

    public ABaseProvider(Class<?> mapperClass, MapperHelper mapperHelper){
        super(mapperClass,mapperHelper);
    }

    /**
     * 根据主键进行逻辑删除
     * @param ms
     * @return
     */
    public String logicalDelete(MappedStatement ms){
        StringBuilder sql=new StringBuilder();
        Class<?> entityClass = getEntityClass(ms);
        sql.append(SqlHelper.updateTable(entityClass, tableName(entityClass)));
        sql.append("<set>");
        sql.append(" enable=0, ");
        sql.append(" update_date='"+ DateUtil.getDateTime()+"' ");
        sql.append("</set>");
//         sql.append(SqlHelper.wherePKColumns(entityClass));
        sql.append("<where>");
        sql.append("id in");
        sql.append("<foreach collection=\"list\" index=\"index\" item=\"item\" open=\"(\" separator=\",\" close=\")\">");
        sql.append("    #{item}");
        sql.append("</foreach>");
        sql.append("</where>");
        return sql.toString();
    }

}

