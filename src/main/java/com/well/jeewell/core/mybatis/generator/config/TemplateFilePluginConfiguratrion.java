package com.well.jeewell.core.mybatis.generator.config;

import org.mybatis.generator.config.PluginConfiguration;

/**
 * Copyright &copy; Well All rights reserved.
 * Author:Well
 * Date:2018-09-06
 * Description:
 */

public class TemplateFilePluginConfiguratrion extends PluginConfiguration {

    /* 配置类型，默认为模板文件插件 */
    private String configurationType="tk.mybatis.mapper.generator.TemplateFilePlugin";
    /* 目标工程，默认为当前工程下的 target/output 目录 */
    private String targetProject="target/output/java";
    /* 模板格式化，默认为 freemarker */
    private String templateFormatter="tk.mybatis.mapper.generator.formatter.FreemarkerTemplateFormatter";

    public TemplateFilePluginConfiguratrion(){
        super.setConfigurationType(this.configurationType);
        this.setTargetProject(targetProject);
        this.setTemplateFormatter(this.templateFormatter);
    }

    public void setTargetProject(String targetProject) {
        super.addProperty("targetProject",targetProject);
    }

    public void setTargetPackage(String targetPackage) {
        super.addProperty("targetPackage",targetPackage);
    }

    public void setTemplatePath(String templatePath) {
        super.addProperty("templatePath",templatePath);
    }

    public void setFileName(String fileName) {
        super.addProperty("fileName",fileName);
    }

    public void setTemplateFormatter(String templateFormatter) {
        super.addProperty("templateFormatter",templateFormatter);
    }
}
