package com.well.jeewell.core.mybatis.generator.config;

/**
 * Copyright &copy; Well All rights reserved.
 * Author:Well
 * Date:2018-09-06
 * Description:controller 模板文件插件配置
 */

public class ControllerTemplateFilePluginConfiguration extends TemplateFilePluginConfiguratrion{

    private String templatePath="template/ftl/controller.java.ftl";
    private String fileName="${tableClass.shortClassName}Controller.java";

    public ControllerTemplateFilePluginConfiguration(){
        super.setTemplatePath(this.templatePath);
        super.setFileName(this.fileName);
    }

}
