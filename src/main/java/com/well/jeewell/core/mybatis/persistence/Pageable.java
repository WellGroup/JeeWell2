package com.well.jeewell.core.mybatis.persistence;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Copyright &copy; Well All rights reserved.
 * Author:Well
 * Date:2018-08-09
 * Description:
 */

public class Pageable {

    @JsonIgnore
    private int pageNum=1; // 页码，默认第一页
    @JsonIgnore
    private int pageSize=10; // 每一页的数量，默认显示10条

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    /**
     * 重写 toString 方法
     * @return
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}

