package com.well.jeewell.core.mybatis.persistence;

import com.well.jeewell.core.mybatis.mapper.genid.GenUUID;
import org.apache.commons.lang3.builder.*;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.Date;

@MappedSuperclass
public abstract class ABaseEntity extends Pageable implements Serializable,Comparable {

    @Id
    @KeySql(genId = GenUUID.class)
    private String id;

    @Column(name="insert_user",columnDefinition="新增人的id",length=32)
    private String insertUser;

    @Column(name="insert_date",columnDefinition="新增时间")
    private Date insertDate;

    @Column(name="update_user",columnDefinition="修改人的id",length=50)
    private String updateUser;

    @Column(name="update_date",columnDefinition="修改时间")
    private Date updateDate;

    @Column(name="enable",columnDefinition="启用标志：0-禁用，1-启用",length=2,nullable = false)
    private Integer enable;

    @Column(name="memo",columnDefinition="备注",length=255)
    private String memo;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getInsertUser() {
        return insertUser;
    }

    public void setInsertUser(String insertUser) {
        this.insertUser = insertUser;
    }

    public Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Integer getEnable() {
        return enable;
    }

    public void setEnable(Integer enable) {
        this.enable = enable;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    /**
     * 重写 toString 方法
     * @return
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

    /**
     * 重写 hashCode 方法
     * @return
     */
    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    /**
     * 重写 equals 方法
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        return EqualsBuilder.reflectionEquals(this, o);
    }

    /**
     * 重写 compareTo 方法
     * @param o
     * @return
     */
    @Override
    public int compareTo(Object o) {
        return CompareToBuilder.reflectionCompare(this, o);
    }

}
