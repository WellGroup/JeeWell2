package com.well.jeewell.core.mybatis.generator.config;

/**
 * Copyright &copy; Well All rights reserved.
 * Author:Well
 * Date:2018-09-04
 * Description:xml 的模板文件插件配置
 */

public class XmlTemplateFilePluginConfiguration extends TemplateFilePluginConfiguratrion{

    private String templatePath="template/ftl/mapper.xml.ftl";
    private String fileName="${tableClass.shortClassName}Mapper.xml";

    public XmlTemplateFilePluginConfiguration(){
        super.setTemplatePath(this.templatePath);
        super.setFileName(this.fileName);
    }

}
