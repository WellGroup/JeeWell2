package com.well.jeewell.core.mybatis.mapper;

import com.well.jeewell.core.mybatis.mapper.provider.ABaseProvider;
import com.well.jeewell.core.mybatis.persistence.ABaseEntity;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.UpdateProvider;
import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.base.BaseUpdateMapper;

import java.util.List;

/**
 * Copyright &copy; Well All rights reserved.
 * Author:Well
 * Date:2017-12-04
 * Description:
 */

public interface ABaseMapper<T extends ABaseEntity> extends Mapper<T>{

    /**
     * 根据主键集合进行逻辑批量删除
     * 将 enable 字段置为0
     * @param idList
     * @return
     */
    @UpdateProvider(type = ABaseProvider.class,method = "dynamicSQL")
    @Options(useCache = false, useGeneratedKeys = false)
    int logicalDelete(List<String> idList);

}

