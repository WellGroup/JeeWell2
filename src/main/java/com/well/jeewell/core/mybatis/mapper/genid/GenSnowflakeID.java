package com.well.jeewell.core.mybatis.mapper.genid;

import tk.mybatis.mapper.genid.GenId;

/**
 * Copyright &copy; Well All rights reserved.
 * Author:Well
 * Date:2018-08-21
 * Description:
 */

public class GenSnowflakeID implements GenId<Long> {

    @Override
    public Long genId(String table, String column) {
        return IDGenerator.snowflakeID();
    }
}
