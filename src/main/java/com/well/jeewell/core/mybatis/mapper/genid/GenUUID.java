package com.well.jeewell.core.mybatis.mapper.genid;

import tk.mybatis.mapper.genid.GenId;

public class GenUUID implements GenId<String> {

    @Override
    public String genId(String table, String column) {
        return IDGenerator.uuid();
    }

}