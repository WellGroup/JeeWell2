package com.well.jeewell.core.mybatis.mapper.genid;

import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;

import java.util.UUID;

/**
 * Copyright &copy; Well All rights reserved.
 * Author:Well
 * Date:2017/1/10
 * Description:
 *  主键 util 类
 */

@Log4j2
public class IDGenerator
{

    /* 推特的雪花算法 id */
    private static SnowflakeID snowflakeID=new SnowflakeID(0,0);

    private IDGenerator(){}

    /**
     * 32位 uuid
     * @return
     */
    public static String uuid() {
        logger.debug("生成没有'-'的 uuid");
        String uuid=UUID.randomUUID().toString();
        return StringUtils.remove(uuid,"-");
    }

    /**
     * 推特的雪花算法id
     * @return
     */
    public static long snowflakeID(){
        logger.debug("生成雪花算法 id");
        return snowflakeID.nextId();
    }

    public static void main(String[] args){
        String uuid= IDGenerator.uuid();
        System.out.println("uuid:"+uuid);
        long snowflakeID= IDGenerator.snowflakeID();
        System.out.println("snowflakeID:"+snowflakeID);
    }

}
