package com.well.jeewell.core.util;

import com.well.jeewell.core.mybatis.mapper.genid.IDGenerator;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.io.FilenameUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

/**
 * Copyright &copy; Well All rights reserved.
 * Author:Well
 * Date:2019-12-27
 * Description:前台上传的复合文件的利用类，TODO：还需优化
 * 文件的处理方法也可以参考 org.apache.commons.io.FileUtils 和 org.apache.commons.io.FilenameUtils
 */

@Log4j2
public class MultipartFileUtil {

    /* 文件的存放目录，该属性只是为了存储到服务器本地 */
    private static final String DIR="C:/Well/Backup/";

    /**
     * 构造函数私有化
     */
    private MultipartFileUtil(){

    }

    /**
     * 转换文件
     * 在 DIR 目录下创建 yyyy 年份目录，在年份目录下创建 MM 月份目录，在月份目录下创建 dd 日目录
     * 最后在日目录中将前台上传的复合文件转换成以 uuid 为文件名的文件
     * 最终的文件全路径为 {DIR}/{year}/{MM}/{dd}/{uuid}.{extension}
     * @param file
     * @throws IOException
     */
    public static void transferTo(MultipartFile file) throws IOException {
        String year= DateUtil.getYear();
        String month=DateUtil.getMonth();
        String day=DateUtil.getDay();
        File dir=new File(DIR+year+"/"+month+"/"+day);
        if(!dir.exists()){
            dir.mkdirs();
        }
        String filename=IDGenerator.uuid()+"."+getExtension(file);
        file.transferTo(new File(dir,filename));
    }

    /**
     * 获取文件的后缀名，不包括 .
     * 例：a.txt 的后缀名为 txt
     * @param file
     * @return
     */
    public static String getExtension(MultipartFile file){
        String filename=file.getOriginalFilename();
        return FilenameUtils.getExtension(filename);
    }

}
