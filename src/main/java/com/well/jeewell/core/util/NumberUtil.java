package com.well.jeewell.core.util;

import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.math.NumberUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;

/**
 * Copyright &copy; Well All rights reserved.
 * Author:Well
 * Date:2020-07-03
 * Description:数字的利用类
 * 数字的处理方法也可以参考 org.apache.commons.lang3.math.NumberUtils 类
 */

@Log4j2
public class NumberUtil {

    /* 构造函数私有化 */
    private NumberUtil(){

    }

    /**
     * 使用英文逗号分组格式化
     * 例：12345678 -> 12,345,678
     * @param i
     * @return
     */
    public static String format(int i){
        return format((long)i);
    }

    /**
     * 使用英文逗号分组格式化
     * 例：12345678 -> 12,345,678
     * @param i
     * @return
     */
    public static String formatNumber(int i){
        return formatNumber((long)i);
    }

    /**
     * 使用英文逗号分组格式化
     * 例：12345678 -> 12,345,678
     * @param i
     * @return
     */
    public static String formatInteger(int i){
        return formatInteger((long)i);
    }

    /**
     * 使用英文逗号分组格式化成百分比
     * 返回的结果为参数乘以100且以百分号结尾
     * 例：12345678 -> 1,234,567,800%
     * @param i
     * @return
     */
    public static String formatPercent(int i){
        return formatPercent((long)i);
    }

    /**
     * 使用英文逗号分组格式化成百分比
     * 返回的结果为参数乘以100且以百分号结尾且四舍五入保留 n 位小数
     * 例：如果 n=0 ，则 12345678 -> 1,234,567,800%
     *     如果 n=1 ，则 12345678 -> 1,234,567,800.0%
     *     如果 n=2 ，则 12345678 -> 1,234,567,800.00%
     * @param i
     * @param n
     * @return
     */
    public static String formatPercent(int i,int n){
        return formatPercent((long)i,n);
    }

    /**
     * 使用英文逗号分组格式化成金额
     * 返回的结果前缀会加上人民币符号，默认保留小数点后两位
     * 例：12345678 -> ￥12,345,678.00
     * @param i
     * @return
     */
    public static String formatCurrency(int i){
        return formatCurrency((long)i);
    }

    /**
     * 使用英文逗号分组格式化成金额
     * 返回的结果前缀会加上人民币符号，默认保留小数点后两位
     * 例：如果 n=0 ，则 12345678 -> ￥12,345,678
     *     如果 n=1 ，则 12345678 -> ￥12,345,678.0
     *     如果 n=2 ，则 12345678 -> ￥12,345,678.00
     * @param i
     * @param n
     * @return
     */
    public static String formatCurrency(int i,int n){
        return formatCurrency((long)i,n);
    }

    /**
     * 使用英文逗号分组格式化
     * 例：123456789 -> 123,456,789
     * @param l
     * @return
     */
    public static String format(long l){
        NumberFormat numberFormat=NumberFormat.getInstance();
        return numberFormat.format(l);
    }

    /**
     * 使用英文逗号分组格式化
     * 例：123456789 -> 123,456,789
     * @param l
     * @return
     */
    public static String formatNumber(long l){
        NumberFormat numberFormat=NumberFormat.getNumberInstance();
        return numberFormat.format(l);
    }

    /**
     * 使用英文逗号分组格式化
     * 例：123456789 -> 123,456,789
     * @param l
     * @return
     */
    public static String formatInteger(long l){
        NumberFormat numberFormat=NumberFormat.getIntegerInstance();
        return numberFormat.format(l);
    }

    /**
     * 使用英文逗号分组格式化成百分比
     * 返回的结果为参数乘以100且以百分号结尾
     * 例：123456789 -> 12,345,678,900%
     * @param l
     * @return
     */
    public static String formatPercent(long l){
        NumberFormat numberFormat=NumberFormat.getPercentInstance();
        return numberFormat.format(l);
    }

    /**
     * 使用英文逗号分组格式化成百分比
     * 返回的结果为参数乘以100且以百分号结尾且四舍五入保留 n 位小数
     * 例：如果 n=0 ，则 123456789 -> 12,345,678,900%
     *     如果 n=1 ，则 123456789 -> 12,345,678,900.0%
     *     如果 n=2 ，则 123456789 -> 12,345,678,900.00%
     * @param l
     * @param n
     * @return
     */
    public static String formatPercent(long l,int n){
        NumberFormat numberFormat=NumberFormat.getPercentInstance();
        /* 保留 n 位小数 */
        numberFormat.setMaximumFractionDigits(n);
        numberFormat.setMinimumFractionDigits(n);
        return numberFormat.format(l);
    }

    /**
     * 使用英文逗号分组格式化成金额
     * 返回的结果前缀会加上人民币符号，默认保留小数点后两位
     * 例：123456789 -> ￥123,456,789.00
     * @param l
     * @return
     */
    public static String formatCurrency(long l){
        NumberFormat numberFormat=NumberFormat.getCurrencyInstance();
        return numberFormat.format(l);
    }

    /**
     * 使用英文逗号分组格式化成金额
     * 返回的结果前缀会加上人民币符号，默认保留小数点后 n 位
     * 例：如果 n=0 ，则 123456789 -> ￥123,456,789
     *     如果 n=1 ，则 123456789 -> ￥123,456,789.0
     *     如果 n=2 ，则 123456789 -> ￥123,456,789.00
     *     如果 n=3 ，则 123456789 -> ￥123,456,789.000
     * @param l
     * @param n
     * @return
     */
    public static String formatCurrency(long l,int n){
        NumberFormat numberFormat=NumberFormat.getCurrencyInstance();
        /* 保留 n 位小数 */
        numberFormat.setMaximumFractionDigits(n);
        numberFormat.setMinimumFractionDigits(n);
        return numberFormat.format(l);
    }

    /**
     * 使用英文逗号分组并且四舍五入保留两位小数格式化
     * 例：123456.789 -> 123,456.79
     *     0.123 -> 0.12
     *     0.125 -> 0.13
     * @param d
     * @return
     */
    public static String format(double d){
        return format(d,2);
    }

    /**
     * 使用英文逗号分组并且四舍五入保留两位小数格式化
     * 例：123456.789 ->123,456.79
     *     0.123 -> 0.12
     *     0.125 -> 0.13
     * @param d
     * @return
     */
    public static String formatNumber(double d){
        return formatNumber(d,2);
    }

    /**
     * 使用英文逗号分组并且四舍五入保留两位小数格式化
     * 例：123456.789 ->123,456.79
     *     0.123 -> 0.12
     *     0.125 -> 0.13
     * @param d
     * @return
     */
    public static String formatInteger(double d){
        return formatInteger(d,2);
    }

    /**
     * 使用英文逗号分组格式化成百分比
     * 返回的结果为参数乘以100且以百分号结尾并四舍五入保留两位小数
     * 例：123456789 -> 12,345,678,900.00%
     *     123456.789 -> 12,345,678.90%
     * @param d
     * @return
     */
    public static String formatPercent(double d){
        return formatPercent(d,2);
    }

    /**
     * 使用英文逗号分组格式化成金额
     * 返回的结果前缀会加上人民币符号并四舍五入保留两位小数
     * 例：12345.6789 -> ￥12,345.68
     *     12345678.9 -> ￥12,345,678.90
     * @param d
     * @return
     */
    public static String formatCurrency(double d){
        return formatCurrency(d,2);
    }

    /**
     * 使用英文逗号分组并且四舍五入保留 n 位小数格式化
     * 例：如果 n=1 ，则 123456.789 -> 123,456.8
     *     如果 n=2 ，则 123456.789 -> 123,456.79
     *     如果 n=3 ，则 123456.789 -> 123,456.789
     * @param d
     * @param n
     * @return
     */
    public static String format(double d,int n){
        BigDecimal b= BigDecimal.valueOf(d);
        d=b.setScale(n,BigDecimal.ROUND_HALF_UP).doubleValue();
        NumberFormat numberFormat=NumberFormat.getInstance();
        return numberFormat.format(d);
    }

    /**
     * 使用英文逗号分组并且四舍五入保留 n 位小数格式化
     * 例：如果 n=1 ，则 12345.6789 -> 12,345.7
     *     如果 n=2 ，则 12345.6789 -> 12,345.68
     *     如果 n=3 ，则 12345.6789 -> 12,345.679
     * @param d
     * @param n
     * @return
     */
    public static String formatNumber(double d,int n){
        BigDecimal b= BigDecimal.valueOf(d);
        d=b.setScale(n,BigDecimal.ROUND_HALF_UP).doubleValue();
        NumberFormat numberFormat=NumberFormat.getNumberInstance();
        return numberFormat.format(d);
    }

    /**
     * 使用英文逗号分组并且四舍五入保留 n 位小数格式化
     * 例：如果 n=1 ，则 12345.6789 -> 12,345.7
     *     如果 n=2 ，则 12345.6789 -> 12,345.68
     *     如果 n=3 ，则 12345.6789 -> 12,345.679
     * @param d
     * @param n
     * @return
     */
    public static String formatInteger(double d,int n){
        BigDecimal b= BigDecimal.valueOf(d);
        d=b.setScale(n,BigDecimal.ROUND_HALF_UP).doubleValue();
        NumberFormat numberFormat=NumberFormat.getIntegerInstance();
        return numberFormat.format(d);
    }

    /**
     * 使用英文逗号分组格式化成百分比
     * 返回的结果为参数乘以100且以百分号结尾并四舍五入保留 n 位小数
     * 例：如果 n=1 ，则 123456.789 -> 12,345,678.9%
     *     如果 n=2 ，则 123456.789 -> 12,345,678.90%
     *     如果 n=3 ，则 123456.789 -> 12,345,678.900%
     * @param d
     * @param n
     * @return
     */
    public static String formatPercent(double d,int n){
        BigDecimal b= BigDecimal.valueOf(d);
        d=b.setScale(n,BigDecimal.ROUND_HALF_UP).doubleValue();
        NumberFormat numberFormat=NumberFormat.getPercentInstance();
        return numberFormat.format(d);
    }

    /**
     * 使用英文逗号分组格式化成金额
     * 返回的结果前缀会加上人民币符号并四舍五入保留 n 位小数
     * 例：如果 n=1 ，则 12345.6789 -> ￥12,345.7
     *     如果 n=2 ，则 12345.6789 -> ￥12,345.68
     *     如果 n=3 ，则 12345.6789 -> ￥12,345.679
     * @param d
     * @param n
     * @return
     */
    public static String formatCurrency(double d,int n){
        BigDecimal b= BigDecimal.valueOf(d);
        d=b.setScale(n,BigDecimal.ROUND_HALF_UP).doubleValue();
        NumberFormat numberFormat=NumberFormat.getCurrencyInstance();
        return numberFormat.format(d);
    }

}
