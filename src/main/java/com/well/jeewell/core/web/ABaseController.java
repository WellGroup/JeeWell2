package com.well.jeewell.core.web;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Copyright &copy; Well All rights reserved.
 * Author:Well
 * Date:2020/01/20
 * Description:业务层抽象基类，对应的泛型实体必须继承 com.well.kernel.mybatis.persistence.ABaseEntity
 */

@Log4j2
public abstract class ABaseController {

    @Autowired
    private Validator validator;

    protected String beanValidator(Object object, Class<?>... groups) {
        try{
            this.validateWithException(validator, object, groups);
        }catch(ConstraintViolationException e){
            List<String> list = this.extractPropertyAndMessageAsList(e, ": ");
            list.add(0, "数据验证失败：");
            String message=this.getMessage(list.toArray(new String[]{}));
            logger.error(e+"\r\n"+message);
            return message;
        }
        return "";
    }

    private void validateWithException(Validator validator, Object object, Class<?>... groups) throws ConstraintViolationException {
        Set constraintViolations = validator.validate(object, groups);
        if (!constraintViolations.isEmpty()) {
            throw new ConstraintViolationException(constraintViolations);
        }
    }

    private String getMessage( String... messages) {
        StringBuilder sb = new StringBuilder();
        for (String message : messages){
//            sb.append(message).append(messages.length>1?"<br/>":"");
            sb.append(message).append(messages.length>1?"\r\n":"");
        }
        return sb.toString();
    }

    /**
     * 辅助方法, 转换ConstraintViolationException中的Set<ConstraintViolations>为List<propertyPath +separator+ message>.
     */
    private List<String> extractPropertyAndMessageAsList(ConstraintViolationException e, String separator) {
        return extractPropertyAndMessageAsList(e.getConstraintViolations(), separator);
    }

    /**
     * 辅助方法, 转换Set<ConstraintViolation>为List<propertyPath +separator+ message>.
     */
    @SuppressWarnings("rawtypes")
    private List<String> extractPropertyAndMessageAsList(Set<? extends ConstraintViolation> constraintViolations, String separator) {
        List<String> errorMessages = new ArrayList<>();
        for (ConstraintViolation violation : constraintViolations) {
            errorMessages.add(violation.getPropertyPath() + separator + violation.getMessage());
        }
        return errorMessages;
    }

}
