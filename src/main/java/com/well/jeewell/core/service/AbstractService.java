package com.well.jeewell.core.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.log4j.Log4j2;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import tk.mybatis.mapper.common.Mapper;

import java.io.Serializable;
import java.util.List;

/**
 * Copyright &copy; Well All rights reserved.
 * Author:Well
 * Date:2017/05/24
 * Description:
 */

@Log4j2
public abstract class AbstractService<T> implements Service<T> {

    @Autowired
    private Mapper<T> mapper;

    /**
     * 新增实体
     *
     * @param entity
     * @return
     */
    public int insert(T entity) {
        return mapper.insert(entity);
    }

    /**
     * 选择新增实体，推荐使用
     * 只新增非空属性
     *
     * @param entity
     * @return
     */
    public int insertSelective(T entity) {
        return mapper.insertSelective(entity);
    }

    /**
     * 根据主键删除实体
     *
     * @param pk
     * @return
     */
    public int delete(Serializable pk) {
        return mapper.deleteByPrimaryKey(pk);
    }

    /**
     * 根据实体非空属性的并集删除实体
     *
     * @param entity
     * @return
     */
    public int delete(T entity) {
        return mapper.delete(entity);
    }

    /**
     * 批量删除所有数据
     *
     * @return
     */
    public int deleteAll() {
        return mapper.delete(null);
    }

    /**
     * 更新实体
     *
     * @param entity
     * @return
     */
    public int update(T entity) {
        return mapper.updateByPrimaryKey(entity);
    }

    /**
     * 选择更新实体，推荐使用
     * 只更新非空属性
     *
     * @param entity
     * @return
     */
    public int updateSelective(T entity) {
        return mapper.updateByPrimaryKeySelective(entity);
    }

    /**
     * 查询全部实体列表
     *
     * @return
     */
    public List<T> selectAll() {
        return mapper.selectAll();
    }

    /**
     * 根据实体非空属性的并集查询实体列表
     *
     * @param entity
     * @return
     */
    public List<T> selectList(T entity) {
        return mapper.select(entity);
    }

    /**
     * 根据实体非空属性的并集查询实体
     * 只返回一个对象
     *
     * @param entity
     * @return
     */
    public T select(T entity) {
        return mapper.selectOne(entity);
    }

    /**
     * 根据主键查询实体
     *
     * @param pk
     * @return
     */
    public T select(Serializable pk) {
        return mapper.selectByPrimaryKey(pk);
    }

    /**
     * 查询所有数量
     * 由于使用 Mybatis Mapper 框架，返回的类型为int
     *
     * @return
     */
    public int selectCount() {
        return mapper.selectCount(null);
    }

    /**
     * 根据对象非空属性的集合查询数量
     * 由于使用 Mybatis Mapper 框架，返回的类型为int
     *
     * @param entity
     * @return
     */
    public int selectCount(T entity) {
        return mapper.selectCount(entity);
    }

    /**
     * 分页查询
     *
     * @param pageNum  页码，从1开始
     * @param pageSize 每一页显示的记录数量
     * @return
     */
    public PageInfo<T> selectByRowBounds(int pageNum, int pageSize) {
        return this.selectByRowBounds(null, pageNum, pageSize);
    }

    /**
     * 根据实体非空属性的并集分页查询
     *
     * @param entity
     * @param pageNum  页码，从0开始
     * @param pageSize 一页显示的条数
     * @return
     */
    public PageInfo<T> selectByRowBounds(T entity, int pageNum, int pageSize) {
        List<T> list = mapper.selectByRowBounds(entity, new RowBounds(pageNum, pageSize));
        PageInfo<T> pageInfo = new PageInfo<T>(list);
        return pageInfo;
    }

    /**
     * 分页查询
     *
     * @param pageNum
     * @param pageSize
     * @return
     */
    public PageInfo<T> selectByPageHelper(int pageNum, int pageSize) {
        return this.selectByPageHelper(null, pageNum, pageSize);
    }

    /**
     * 根据实体非空属性的并集进行分页查询
     *
     * @param entity
     * @param pagNum
     * @param pageSize
     * @return
     */
    public PageInfo<T> selectByPageHelper(T entity, int pagNum, int pageSize) {
        PageHelper.startPage(pagNum, pageSize);
        List<T> list = mapper.select(entity);
        PageInfo<T> pageInfo = new PageInfo<T>(list);
        return pageInfo;
    }
}
