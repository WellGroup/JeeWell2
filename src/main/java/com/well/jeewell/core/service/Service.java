package com.well.jeewell.core.service;

import com.github.pagehelper.PageInfo;

import java.io.Serializable;
import java.util.List;

/**
 * Copyright &copy; Well All rights reserved.
 * Author:Well
 * Date:2017/06/07
 * Description: 业务逻辑接口，实现简单的单表业务逻辑
 */

public interface Service<T> 
{
	/**
	 * 新增
	 * @param entity
	 * @return
	 */
	int insert(T entity);

	/**
	 * 选择性新增，只新增属性不为空的字段
	 * @param entity
	 * @return
	 */
	int insertSelective(T entity);

	/**
	 * 根据主键删除
	 * @param pk
	 * @return
	 */
	int delete(Serializable pk);

	/**
	 * 根据对象属性集合的并集进行删除
	 * @param t
	 * @return
	 */
	int delete(T t);

	/**
	 * 删除所有
	 * @return
	 */
	int deleteAll();

	/**
	 * 根据主键更新
	 * @param t
	 * @return
	 */
	int update(T t);

	/**
	 * 根据主键选择性更新
	 * @param t
	 * @return
	 */
	int updateSelective(T t);

	/**
	 * 查询所有数据
	 * @return
	 */
	List<T> selectAll();

	/**
	 * 根据对象属性值的并集查询列表
	 * @param t
	 * @return
	 */
	List<T> selectList(T t);

	/**
	 * 根据对象属性值的并集查询一个对象
	 * @param t
	 * @return
	 */
	T select(T t);

	/**
	 * 根据主键查询一个对象
	 * @param pk
	 * @return
	 */
	T select(Serializable pk);

	/**
	 * 根据 RowBounds 分页查询
	 * @param t 对象
	 * @param pageNum 页码
	 * @param pageSize 每一页显示的记录数量
	 * @return
	 */
	PageInfo<T> selectByRowBounds(T t,int pageNum, int pageSize);

	/**
	 * 根据 PageHelper 分页查询
	 * @param t 对象
	 * @param pageNum 页码
	 * @param pageSize 每一特显示的记录数量
	 * @return
	 */
	PageInfo<T> selectByPageHelper(T t,int pageNum, int pageSize);

	/**
	 * 根据对象查询数量
	 * @param t
	 * @return
	 */
	int selectCount(T t);
}
