package com.well.jeewell.core.exception;

/**
 * Copyright &copy; Well All rights reserved.
 * Author:Well
 * Date:2018-08-13
 * Description:
 */

public class BusinessException extends RuntimeException{

    public BusinessException(){
        super("业务异常！");
    }

    public BusinessException(String message){
        super(message);
    }

}
