package com.well.jeewell.core.exception;

import com.well.jeewell.core.spring.web.ResponseData;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.servlet.NoHandlerFoundException;

/**
 * 异常处理
 * @author Well
 *
 */

@Log4j2
@RestControllerAdvice
public class Handler {

	/**
	 * 业务异常处理，自定义异常
	 * @param e
	 * @return
	 */
	@ExceptionHandler(value = BusinessException.class)
	public ResponseData businessException(BusinessException e){
		logger.error(e);
		return new ResponseData(false,-1,e.getMessage());
	}
	
	/**
	 * 无效请求异常处理，404
	 * @param e
	 * @return
	 */
	@ExceptionHandler(value = NoHandlerFoundException.class)
	public ResponseData noHandlerFoundException(NoHandlerFoundException e){
		logger.error(e);
		return new ResponseData(false,404,"无效的请求！");
	}

	/**
	 * 请求方法异常处理，405
	 * @param e
	 * @return
	 */
	@ExceptionHandler(value = HttpRequestMethodNotSupportedException.class)
	public ResponseData httpRequestMethodNotSupportedException(HttpRequestMethodNotSupportedException e){
		logger.error(e);
		return new ResponseData(false,405,"请求方法错误！");
	}
	
	/**
	 * 空指针异常处理
	 * @param e
	 * @return
	 */
	@ExceptionHandler(value = NullPointerException.class)
	public ResponseData nullPointerException(NullPointerException e){
		logger.error(e);
		return new ResponseData(false,-1,"空指针异常！");
	}

    /**
     * 文件大小超出限制处理
     * @param e
     * @return
     */
	@ExceptionHandler(value = MaxUploadSizeExceededException.class)
    public ResponseData maxUploadSizeExceededException(MaxUploadSizeExceededException e){
        logger.error(e);
        return new ResponseData(false,-1,"文件大小超出限制");
    }

	/**
	 * 默认异常处理
	 * @param e
	 * @return
	 */
	@ExceptionHandler(value = Exception.class)
	public ResponseData exception(Exception e){
		logger.error(e);
		return new ResponseData(false,-1,"系统异常！");
	}


}
