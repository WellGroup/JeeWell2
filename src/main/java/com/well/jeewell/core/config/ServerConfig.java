package com.well.jeewell.core.config;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.context.WebServerInitializedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Configuration;

import javax.servlet.ServletContext;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Copyright &copy; Well All rights reserved.
 * Author:Well
 * Date:2020-01-18
 * Description:服务配置
 */

@Log4j2
@Configuration
public class ServerConfig implements ApplicationListener<WebServerInitializedEvent> {

    private int port;

    @Autowired
    private ServletContext servletContext;

    @Override
    public void onApplicationEvent(WebServerInitializedEvent event) {
        this.port=event.getWebServer().getPort();
        logger.info("平台访问地址："+this.getURL());
    }

    public String getURL(){
        return "http://"+this.getHostAddress()+":"+this.getPort()+this.getContextPath();
    }

    public String getHostAddress(){
        try{
            return InetAddress.getLocalHost().getHostAddress();
        }catch (UnknownHostException e){
            logger.error(e);
        }
        return null;
    }

    public int getPort(){
        return port;
    }

    public String getContextPath(){
        return servletContext.getContextPath();
    }

}
