package com.well.jeewell.core.spring.web;

/**
 * Copyright &copy; Well All rights reserved.
 * Author:Well
 * Date:2016-12-12
 * Description:响应数据类
 */

public class ResponseData extends BaseResponse {

    private Object data;

    public ResponseData() {
        super();
    }

    public ResponseData(boolean success) {
        super(success);
    }

    public ResponseData(int code) {
        super(code);
    }

    public ResponseData(String message) {
        super(message);
    }

    public ResponseData(boolean success, int code) {
        super(success, code);
    }

    public ResponseData(boolean success, String message) {
        super(success, message);
    }

    public ResponseData(boolean success, Object data) {
        super(success);
        this.data = data;
    }

    public ResponseData(int code, String message) {
        super(code, message);
    }

    public ResponseData(int code, Object data) {
        super(code);
        this.data = data;
    }

    public ResponseData(String message, Object data) {
        super(message);
        this.data = data;
    }

    public ResponseData(boolean success, int code, String message) {
        super(success, code, message);
    }

    public ResponseData(boolean success, int code, Object data) {
        super(success, code);
        this.data = data;
    }

    public ResponseData(boolean success, String message, Object data) {
        super(success, message);
        this.data = data;
    }

    public ResponseData(int code, String message, Object data) {
        super(code, message);
        this.data = data;
    }

    public ResponseData(boolean success, int code, String message, Object data) {
        super(success, code, message);
        this.data = data;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}