package com.well.jeewell.core.spring.web;

/**
 * Copyright &copy; Well All rights reserved.
 * Author:Well
 * Date:2018-08-18
 * Description:响应实体类
 */

public class ResponseEntity<T> extends BaseResponse {

    private T data;

    public ResponseEntity() {
        super();
    }

    public ResponseEntity(boolean success) {
        super(success);
    }

    public ResponseEntity(int code) {
        super(code);
    }

    public ResponseEntity(String message) {
        super(message);
    }

    public ResponseEntity(boolean success, int code) {
        super(success, code);
    }

    public ResponseEntity(boolean success, String message) {
        super(success, message);
    }

    public ResponseEntity(boolean success, T data) {
        super(success);
        this.data = data;
    }

    public ResponseEntity(int code, String message) {
        super(code, message);
    }

    public ResponseEntity(int code, T data) {
        super(code);
        this.data = data;
    }

    public ResponseEntity(String message, T data) {
        super(message);
        this.data = data;
    }

    public ResponseEntity(boolean success, int code, String message) {
        super(success, code, message);
    }

    public ResponseEntity(boolean success, int code, T data) {
        super(success, code);
        this.data = data;
    }

    public ResponseEntity(boolean success, String message, T data) {
        super(success, message);
        this.data = data;
    }

    public ResponseEntity(int code, String message, T data) {
        super(code, message);
        this.data = data;
    }

    public ResponseEntity(boolean success, int code, String message, T data) {
        super(success, code, message);
        this.data = data;
    }

}
