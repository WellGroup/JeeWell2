package com.well.jeewell.core.spring.web;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;

/**
 * Copyright &copy; Well All rights reserved.
 * Author:Well
 * Date:2018-08-17
 * Description:基本响应类
 */

public class BaseResponse implements Serializable {

    private boolean success = true;
    private int code = 1;
    private String message;

    public BaseResponse() {
        this.message = "操作成功！";
    }

    public BaseResponse(boolean success) {
        this.success = success;
    }

    public BaseResponse(int code) {
        this.code = code;
    }

    public BaseResponse(String message) {
        this.message = message;
    }

    public BaseResponse(boolean success, int code) {
        this.success = success;
        this.code = code;
    }

    public BaseResponse(boolean success, String message) {
        this.success = success;
        this.message = message;
    }

    public BaseResponse(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public BaseResponse(boolean success, int code, String message) {
        this.success = success;
        this.code = code;
        this.message = message;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

}
