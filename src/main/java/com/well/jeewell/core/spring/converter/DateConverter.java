package com.well.jeewell.core.spring.converter;

import com.well.jeewell.core.util.DateUtil;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * Copyright &copy; Well All rights reserved.
 * Author:Well
 * Date:2017/1/10
 * Description:
 * 日期转换器，将前台的字符串日期转换成 java.util.Date
 */

@Log4j2
public class DateConverter implements Converter<String ,Date>
{

    /**
     * 将字符串类型的日期转换为 java.util.Date 类型
     * @param s
     * @return
     */
    @Override
    public Date convert(String s) {
        return DateUtil.parse(s);
    }
}
