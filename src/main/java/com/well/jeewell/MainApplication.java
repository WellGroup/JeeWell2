package com.well.jeewell;

import com.well.jeewell.core.config.ServerConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.util.unit.DataSize;
import org.springframework.util.unit.DataUnit;

import javax.servlet.MultipartConfigElement;

@EnableAsync // 开启对 @Async 注解的解析
@EnableScheduling // 开启对 @Scheduled 注解的解析
@SpringBootApplication
public class MainApplication {

    public static void main(String[] args) {
        SpringApplication.run(MainApplication.class, args);
        println();
    }

    private static void println(){
        System.out.println();
        System.out.println("********************************************************");
        System.out.println("*                      启动成功                        *");
        System.out.println("********************************************************");
        System.out.println();
    }

}
