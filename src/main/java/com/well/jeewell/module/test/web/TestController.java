package com.well.jeewell.module.test.web;

import com.well.jeewell.core.spring.web.ResponseData;
import com.well.jeewell.core.util.DateUtil;
import com.well.jeewell.core.util.MultipartFileUtil;
import com.well.jeewell.core.util.WebUtil;
import com.well.jeewell.core.web.ABaseController;
import com.well.jeewell.module.test.properties.TestProperties;
import com.well.jeewell.module.user.entity.UserInfo;
import com.well.jeewell.module.user.service.UserInfoService;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Copyright &copy; Well All rights reserved.
 * Author:Well
 * Date:2019-03-27
 * Description:测试控制层
 */

@RestController
@RequestMapping("/test")
@Log4j2
public class TestController extends ABaseController {

    /**
     * 测试跨域
     * @return
     */
    @CrossOrigin("*")
    @GetMapping("/cors")
    public ResponseData cors(){
        ResponseData responseData=new ResponseData();
        responseData.setMessage("测试跨域！");
        return responseData;
    }

    /**
     * 日期转换测试
     * @param date
     * @return
     */
    @GetMapping("/convert/{date}")
    public String test(@PathVariable Date date) {
        logger.info(date);
        return "All is Well !";
    }

    @GetMapping("/getDate")
    public Date getDate(){
        return new Date();
    }

    @PostMapping("/method")
    public String method(){
        return "This is post method";
    }

    /**
     * 配置属性测试
     */
    @Autowired
    private TestProperties testProperties;
    @GetMapping("/properties")
    public TestProperties testProperties(){
        logger.info(this.testProperties);
        return this.testProperties;
    }

    /******************************* 文件上传测试 *******************************/

    /**
     * 单文件上传测试
     * 参数中 @RequestParam("file") 此处的"file"对应的就是html 中 name="file" 的 input 标签
     * 真正写入文件的是 apache commons-io 中的 FileUtils.copyInputStreamToFile(inputStream,file)
     * @param file
     * @return
     */
    @PostMapping("/file/upload")
    public ResponseData upload(@RequestParam("file") MultipartFile file) throws IOException {
        ResponseData responseData=new ResponseData();
        logger.info("文件类型："+file.getContentType());
        logger.info("文件名称："+file.getOriginalFilename());
        logger.info("文件大小："+file.getSize());
        String year= DateUtil.getYear();
        String month=DateUtil.getMonth();
        String day=DateUtil.getDay();
        File dir=new File("C:\\Well\\Backup\\"+year+"\\"+month+"\\"+day);
        if(!dir.exists()){
            dir.mkdirs();
        }
        file.transferTo(new File(dir,file.getOriginalFilename()));
        return responseData;
    }

    /**
     * 多文件上传测试
     * 参数中 @RequestParam("file") 此处的"file"对应的就是html 中 name="file" 的 input 标签
     * 真正写入文件的是 apache commons-io 中的 FileUtils.copyInputStreamToFile(inputStream,file)
     * @param files
     * @return
     * @throws IOException
     */
    @PostMapping("/file/uploads")
    public ResponseData uploads(@RequestParam("file") MultipartFile[] files) throws IOException {
        ResponseData responseData=new ResponseData();
        if (files == null || files.length == 0) {
            responseData.setSuccess(false);
            responseData.setMessage("操作失败！");
            return responseData;
        }
        for(MultipartFile file:files){
            logger.info("文件类型："+file.getContentType());
            logger.info("文件名称："+file.getOriginalFilename());
            logger.info("文件大小："+file.getSize());
            String year= DateUtil.getYear();
            String month=DateUtil.getMonth();
            String day=DateUtil.getDay();
            File dir=new File("C:\\Well\\Backup\\"+year+"\\"+month+"\\"+day);
            if(!dir.exists()){
                dir.mkdirs();
            }
            file.transferTo(new File(dir,file.getOriginalFilename()));
        }
        return responseData;
    }

    /**
     * Base64 文件上传
     * @return
     */
    @PostMapping("/file/uploadByBase64")
    public ResponseData uploadByBase64(){
        ResponseData responseData=new ResponseData();
        // TODO:未实现
        return responseData;
    }

    /**
     * 测试自定义的类 com.well.jeewell.core.util.MultipartFileUtil
     * @param file
     * @return
     */
    @PostMapping("/file/testMultipartFileUtil")
    public ResponseData testFileUtil(MultipartFile file) throws IOException {
        ResponseData responseData=new ResponseData();
        String extension= MultipartFileUtil.getExtension(file);
        logger.info(extension);
        MultipartFileUtil.transferTo(file);
        return responseData;
    }

    /******************************* 参数校验测试 *******************************/

    @Autowired
    private UserInfoService userInfoService;

    @Autowired
    private Validator validator;

    /**
     * 测试参数校验
     * @return
     */
    @RequestMapping(value = "testValidator")
    public ResponseData testValidator(){
        ResponseData responseData=new ResponseData();

        UserInfo userInfo=new UserInfo();
        String result=super.beanValidator(userInfo);
        if (StringUtils.isNotBlank(result)){
            responseData.setSuccess(false);
            responseData.setMessage(result);
            return responseData;
        }
        userInfoService.save(userInfo);

        return responseData;
    }

}
