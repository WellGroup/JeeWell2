package com.well.jeewell.module.test.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Data // lombok 插件，此注解会为类的所有属性自动生成 setter、getter、equals、canEqual、hashCode、toString 方法，如为 final 属性，则不会为该属性生成 setter 方法
@Component // 使用 @ConfigurationProperties 必须要使用 @Component
@PropertySource("classpath:test.properties") // 配置文件，如果是在 application.properties 中自定义配置内容则不需要该注解
@ConfigurationProperties(prefix = "test") // 配置属性前缀
public class TestProperties {

    private String name;
    private String email;
    private String content;
    private int number1;
    private Integer number2;
    private String blank; // 空字符串
    private String empty; // 空

}
