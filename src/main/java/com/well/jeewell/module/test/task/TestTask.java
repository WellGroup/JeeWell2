package com.well.jeewell.module.test.task;

import com.well.jeewell.core.util.DateUtil;
import lombok.extern.log4j.Log4j2;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Log4j2
@Component
public class TestTask {

//    @Async
//    @Scheduled(cron = "0/1 * * * * *")
    public void scheduled1() throws InterruptedException {
        Thread.sleep(3000);
        logger.info("scheduled1 每1秒执行一次："+DateUtil.getDateTime());
    }

//    @Scheduled(fixedRate = 1000)
    public void scheduled2() throws InterruptedException {
        Thread.sleep(3000);
        logger.info("scheduled2 每1秒执行一次："+DateUtil.getDateTime());
    }

//    @Scheduled(fixedDelay = 3000)
    public void scheduled3() throws InterruptedException {
        Thread.sleep(5000);
        logger.info("scheduled3 上次执行完毕后隔3秒继续执行："+DateUtil.getDateTime());
    }

}
