package com.well.jeewell.module;

import com.well.jeewell.core.spring.web.ResponseData;
import com.well.jeewell.core.util.WebUtil;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * Copyright &copy; Well All rights reserved.
 * Author:Well
 * Date:2019-03-14
 * Description:首页 controller 类
 */

@Controller
@Log4j2
public class IndexController {

    @RequestMapping({"","/index"})
    public ModelAndView index(ModelMap modelMap,HttpServletRequest request){
        String ip= WebUtil.getIP(request);
        logger.info("访问IP："+ip);
        modelMap.put("key","All is Well!");
        return new ModelAndView("index",modelMap);
    }

}
