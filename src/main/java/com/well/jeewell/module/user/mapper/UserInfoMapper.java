package com.well.jeewell.module.user.mapper;

import com.well.jeewell.core.mybatis.mapper.ABaseMapper;
import com.well.jeewell.module.user.entity.UserInfo;

@org.apache.ibatis.annotations.Mapper
public interface UserInfoMapper extends ABaseMapper<UserInfo> {
}
