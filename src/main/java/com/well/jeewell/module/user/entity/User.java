package com.well.jeewell.module.user.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.well.jeewell.core.mybatis.mapper.genid.GenUUID;
import org.apache.commons.lang3.builder.*;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Copyright &copy; Well All rights reserved.
 * Author:Well
 * Date:2017-11-28
 * Description:
 */

@Table(name="user")
public final class User implements Serializable,Comparable{
    @Id
    @KeySql(genId = GenUUID.class)
    private String id;

    @Column
    private String username;

    @JsonIgnore
    @Column
    private String password;

    @Column
    private String realname;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRealname() {
        return realname;
    }

    public void setRealname(String realname) {
        this.realname = realname;
    }

    /**
     * 重写 toString 方法
     * @return
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

    /**
     * 重写 hashCode 方法
     * @return
     */
    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    /**
     * 重写 equals 方法
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        return EqualsBuilder.reflectionEquals(this, o);
    }

    /**
     * 重写 compareTo 方法
     * @param o
     * @return
     */
    @Override
    public int compareTo(Object o) {
        return CompareToBuilder.reflectionCompare(this, o);
    }
}
