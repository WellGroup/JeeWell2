package com.well.jeewell.module.user.entity;

import com.well.jeewell.core.mybatis.persistence.ABaseEntity;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Table(name = "user_info")
public class UserInfo extends ABaseEntity {

    /* user表的id */
    @Column(name="user_id",columnDefinition="user表的id",length=32,nullable=false)
    @NotNull
    private String userId;

    /* 性别，1-男，2-女 */
    @Column(name="gender",columnDefinition="性别，1-男，2-女",length=10,nullable=false)
    @NotNull
    private Integer gender;

    /* 生日 */
    @Column(name="birthday",columnDefinition="生日")
    private Date birthday;

    /**
     * 获取 user表的id
     * @return userId user表的id
     */
    public String getUserId() {
        return userId;
    }

    /**
     * 设置 user表的id
     * @param userId user表的id
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * 获取 性别，1-男，2-女
     * @return gender 性别，1-男，2-女
     */
    public Integer getGender() {
        return gender;
    }

    /**
     * 设置 性别，1-男，2-女
     * @param gender 性别，1-男，2-女
     */
    public void setGender(Integer gender) {
        this.gender = gender;
    }

    /**
     * 获取 生日
     * @return birthday 生日
     */
    public Date getBirthday() {
        return birthday;
    }

    /**
     * 设置 生日
     * @param birthday 生日
     */
    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

}
