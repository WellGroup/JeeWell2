package com.well.jeewell.module.user.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.well.jeewell.core.service.ABaseService;
import com.well.jeewell.module.user.entity.UserInfo;
import com.well.jeewell.module.user.mapper.UserInfoMapper;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;

/**
 * Copyright &copy; Well All rights reserved.
 * Author:
 * Date:2018-9-6
 * Description:
 */

@Log4j2
@Service
public class UserInfoService extends ABaseService<UserInfo> {

    @Autowired
    private UserInfoMapper userInfoMapper;

    /**
     * 获取分页数据
     *
     * @param userInfo
     * @return
     */
    public PageInfo<UserInfo> selectPageInfo(UserInfo userInfo) {
        int pageNum = userInfo.getPageNum();
        int pageSize = userInfo.getPageSize();
        PageHelper.startPage(pageNum, pageSize); // 开始分页
        // TODO:根据实际情况进行查询
        List<UserInfo> list = super.selectList(userInfo);
        PageInfo<UserInfo> pageInfo = new PageInfo<>(list);
        return pageInfo;
    }

    /**
     * 根据主键获取详情
     *
     * @param id 主键
     * @return
     */
    @Override
    public UserInfo select(String id) {
        if (StringUtils.isBlank(id)) {
            return null;
        }
        return super.select(id);
    }

    /**
     * 保存。当主键为null时，新增，否则，更新
     *
     * @param userInfo
     * @return
     */
    @Transactional
    public int save(UserInfo userInfo) {
        if (StringUtils.isBlank(userInfo.getId())) {
            return super.insertSelective(userInfo);
        }
        return super.updateSelective(userInfo);
    }

    /**
     * 逻辑删除
     *
     * @param ids 多个主键，以","分割
     * @return
     */
    public int logicalDelete(String ids) {
        if (StringUtils.isBlank(ids)) {
            return 0;
        }
        String[] ss = StringUtils.split(ids, ",");
        List<String> idList = Arrays.asList(ss);
        return super.logicalDelete(idList);
    }

}

