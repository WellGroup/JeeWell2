package com.well.jeewell.module.user.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.well.jeewell.core.service.AbstractService;
import com.well.jeewell.module.user.entity.User;
import com.well.jeewell.module.user.mapper.UserMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

import java.util.List;

/**
 * Copyright &copy; Well All rights reserved.
 * Author:Well
 * Date:2017-11-28
 * Description:
 */

@Service
public class UserService extends AbstractService<User> {

    @Autowired
    private UserMapper userMapper;

    public PageInfo<User> selectPageInfo(User user){
        return null;
    }

}
