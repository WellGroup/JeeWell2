package com.well.jeewell.module.user.mapper;

import com.well.jeewell.module.user.entity.User;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

/**
 * Copyright &copy; Well All rights reserved.
 * Author:Well
 * Date:2017-11-28
 * Description:
 */

@org.apache.ibatis.annotations.Mapper
public interface UserMapper extends Mapper<User> {

    List<User> selectUserList();

}
