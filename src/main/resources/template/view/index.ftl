<#assign ctx=request.getContextPath()>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>首页</title>
    <#-- 在浏览器上面显示图标 -->
    <link rel="shortcut icon" href="${ctx}/static/favicon.ico" />
    <link rel="bookmark" href="${ctx}/static/favicon.ico" />
    <script src="http://code.jquery.com/jquery-2.1.1.min.js"></script>
</head>
<body>
    <h1>${key}</h1>

    <h2>单一文件上传示例</h2>
    <div>
        <form method="POST" enctype="multipart/form-data" action="${ctx}/test/file/upload">
            <p>
                文件1：<input type="file" name="file"/>
                <input type="submit" value="上传"/>
            </p>
        </form>
    </div>

    <hr/>
    <h2>批量文件上传示例</h2>

    <div>
        <form method="POST" enctype="multipart/form-data" action="${ctx}/test/file/uploads">
            <p>
                文件1：<input type="file" name="file"/>
            </p>
            <p>
                文件2：<input type="file" name="file"/>
            </p>
            <p>
                <input type="submit" value="上传"/>
            </p>
        </form>
    </div>

    <hr/>
    <h2>Base64文件上传(未实现)</h2>
    <div>
        <form method="POST" action="">
            <p>
                BASE64编码：<textarea name="base64" rows="10" cols="80"></textarea>
                <input type="submit" value="上传"/>
            </p>
        </form>
    </div>

    <hr/>
    <h2>自定义类 FileUtil 测试</h2>
    <div>
        <form method="POST" enctype="multipart/form-data" action="${ctx}/test/file/testMultipartFileUtil">
            <p>
                文件：<input type="file" name="file"/>
                <input type="submit" value="上传"/>
            </p>
        </form>
    </div>

    <hr/>
    <h2>参数校验</h2>
    <div>
        <input type="button" id="testValidator" value="确定" />
        <br/>
        <label>返回结果：</label>
        <textarea rows="5" cols="50" id="testValidatorResult">

        </textarea>
    </div>

    <script type="text/javascript">
        $(function(){

            /* 测试跨域 */
            $.get("http://192.168.11.131:8080/jeewell/test/cors",function(data){
                console.log(data);
            });

            $("#testValidator").click(function(){
                $.post("${ctx}/test/testValidator",{},function(data){
                    var message=data.message;
                    $("#testValidatorResult").text(message);
                });
            });

        });

    </script>
</body>
</html>