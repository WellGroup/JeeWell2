package ${package};

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.well.kernel.service.ABaseService;
import ${tableClass.fullClassName};
import ${tableClass.packageName?replace(".entity",".mapper")}.${tableClass.shortClassName}Mapper;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;

<#assign dateTime = .now>
/**
* Copyright &copy; Well All rights reserved.
* Author:
* Date:${dateTime?date}
* Description:
*/

@Service
public class ${tableClass.shortClassName}Service extends ABaseService<${tableClass.shortClassName}> {

    private static Logger logger=Logger.getLogger(${tableClass.shortClassName}.class);

    @Autowired
    private ${tableClass.shortClassName}Mapper ${tableClass.variableName}Mapper;

    /**
     * 获取分页数据
     * @param ${tableClass.variableName}
     * @return
     */
    public PageInfo<${tableClass.shortClassName}> selectPageInfo(${tableClass.shortClassName} ${tableClass.variableName}){
        int pageNum=${tableClass.variableName}.getPageNum();
        int pageSize=${tableClass.variableName}.getPageSize();
        PageHelper.startPage(pageNum,pageSize); // 开始分页
        // TODO:根据实际情况进行查询
        List<${tableClass.shortClassName}> list=super.selectList(${tableClass.variableName});
        PageInfo<${tableClass.shortClassName}> pageInfo=new PageInfo<>(list);
        return pageInfo;
    }

    /**
     * 根据主键获取详情
     * @param id 主键
     * @return
     */
    @Override
    public ${tableClass.shortClassName} select(String id){
        if(StringUtils.isBlank(id)){
            return null;
        }
        return super.select(id);
    }

    /**
     * 保存。当主键为null时，新增，否则，更新
     * @param ${tableClass.variableName}
     * @return
     */
    @Transactional
    public int save(${tableClass.shortClassName} ${tableClass.variableName}){
        if(StringUtils.isBlank(${tableClass.variableName}.getId())){
            return super.insertSelective(${tableClass.variableName});
        }
        return super.updateSelective(${tableClass.variableName});
    }

    /**
     * 逻辑删除
     * @param ids 多个主键，以","分割
     * @return
     */
    public int logicalDelete(String ids){
        if(StringUtils.isBlank(ids)){
            return 0;
        }
        String[] ss=StringUtils.split(ids,",");
        List<String> idList=Arrays.asList(ss);
        return super.logicalDelete(idList);
    }

}