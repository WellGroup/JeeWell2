package ${package};

import com.well.SpringTest;
import ${tableClass.fullClassName};
import ${tableClass.packageName?replace(".entity",".service")}.${tableClass.shortClassName}Service;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

<#assign dateTime = .now>
/**
* Copyright &copy; Well All rights reserved.
* Author:
* Date:${dateTime?date}
* Description:TODO:单元测试类，放在测试目录中
*/

public class ${tableClass.shortClassName}ServiceTest extends SpringTest{

    @Autowired
    private ${tableClass.shortClassName}Service ${tableClass.variableName}Service;

    @Test
    public void test(){
        // TODO:单元测试
    }

}