package ${package};

import com.github.pagehelper.PageInfo;
import com.well.kernel.spring.web.ResponseData;
import com.well.kernel.spring.web.easyui.Datagrid;
import ${tableClass.fullClassName};
import ${tableClass.packageName?replace(".entity",".service")}.${tableClass.shortClassName}Service;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

<#assign dateTime = .now>
/**
* Copyright &copy; Well All rights reserved.
* Author:
* Date:${dateTime?date}
* Description:
*/

@Controller
@RequestMapping("/${tableClass.variableName}")
public class ${tableClass.shortClassName}Controller{

    @Autowired
    private ${tableClass.shortClassName}Service ${tableClass.variableName}Service;

    /**
     * 进入首页页面
     * @param modelMap
     * @return
     */
    @RequestMapping("/index")
    public ModelAndView index(ModelMap modelMap){
        // TODO:请输入页面
        return new ModelAndView("",modelMap);
    }

    /**
     * 获取 easyui 的 datagrid 分页数据
     * @param ${tableClass.variableName}
     * @return
     */
    @RequestMapping("/datagrid")
    @ResponseBody
    public Datagrid<${tableClass.shortClassName}> datagrid(${tableClass.shortClassName} ${tableClass.variableName}){
        PageInfo<${tableClass.shortClassName}> pageInfo=${tableClass.variableName}Service.selectPageInfo(${tableClass.variableName});
        Datagrid<${tableClass.shortClassName}> datagrid=new Datagrid<${tableClass.shortClassName}>(pageInfo);
        return datagrid;
    }

    /**
     * 进入详情页面
     * @param id 主键
     * @param modelMap
     * @return
     */
    @RequestMapping("/detail")
    public ModelAndView detail(String id,ModelMap modelMap){
        ${tableClass.shortClassName} ${tableClass.variableName}=${tableClass.variableName}Service.select(id);
        modelMap.put("${tableClass.variableName}",${tableClass.variableName});
        // TODO:进入详情页面
        return new ModelAndView("",modelMap);
    }

    /**
     * 保存
     * @param ${tableClass.variableName}
     * @return
     */
    @RequestMapping("/save")
    @ResponseBody
    public ResponseData save(${tableClass.shortClassName} ${tableClass.variableName}){
        int result=${tableClass.variableName}Service.save(${tableClass.variableName});
        if(result==0){
            return new ResponseData(false,"保存失败！");
        }
        return new ResponseData(true,"保存成功！");
    }

    /**
     * 逻辑删除
     * @param ids 多个主键，以","分割
     * @return
     */
    @RequestMapping("/logicalDelete")
    @ResponseBody
    public ResponseData logicalDelete(String ids){
        int result=${tableClass.variableName}Service.logicalDelete(ids);
        if(result==0){
            return new ResponseData(false,"删除失败！");
        }
        return new ResponseData(true,"删除【"+result+"】条数据成功！");
    }

}