package ${package};

import com.well.kernel.mybatis.mapper.ABaseMapper;
import ${tableClass.fullClassName};

<#assign dateTime = .now>
/**
* Copyright &copy; Well All rights reserved.
* Author:
* Date:${dateTime?date}
* Description:
*/

public interface ${tableClass.shortClassName}Mapper extends ABaseMapper<${tableClass.shortClassName}> {

}